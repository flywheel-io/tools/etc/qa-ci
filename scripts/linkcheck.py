#!/usr/bin/env python
"""Check source files for dead links."""

import os
import random
import re
import shlex
import sys
import typing as t
from argparse import ArgumentParser
from collections import defaultdict
from concurrent.futures import ThreadPoolExecutor
from dataclasses import dataclass
from pathlib import Path
from urllib.parse import urlparse

from diskcache import Cache
from fw_http_client import ClientError, HttpClient, ServerError
from fw_utils import quantify as q

# set to true to bump verbosity
debug = os.environ.get("DEBUG")

# define the args and options
parser = ArgumentParser(description=__doc__)
parser.add_argument(
    "files",
    nargs="+",
    metavar="FILE",
    help="source files to check links in",
)
parser.add_argument(
    "-i",
    "--ignore",
    action="append",
    default=[],
    metavar="RE",
    help="ignore URLs matching regex",
)

# ansi color codes
bold, red, green, reset = "\033[1m", "\033[1;31m", "\033[1;32m", "\033[0m"
# constant regexes
link_re = re.compile(r"(https?://[-a-z0-9@:%_\+.~#?&/=]+)(?:\s|\)|\]|$)", flags=re.I)
helm_re = re.compile(r"helm|chart|bitnami", flags=re.I)
mail_re = re.compile(r"([a-z0-9]+[.-_])*[a-z0-9]+@[a-z0-9-]+(\.[a-z]{2,})+", flags=re.I)
md_link_re = re.compile(r"\[.*?\]\((.*?)\)")
md_header_re = re.compile(r"#+ (.+)")
# static config - top level domains and hostnames to ignore
ignore_tlds = {"local", "test"}
ignore_hosts = {"harbor-core.flywheel", "helm.dev.flywheel.io", "local.flywheel.io"}
# http client, cache and markdown header lookup table (shared across threads)
http = HttpClient()
# use browser-like headers to side-step bot-haters like microsoft/github
browser_headers = http.get("https://headers.scrapeops.io/v1/browser-headers")
http.headers.update(random.choice(browser_headers.result))
# use github token for gh requests if available
github_re = re.compile(r"https://(api\.)?github\.com")
github_token = os.getenv("GITHUB_TOKEN")

cache = Cache(".cache/linkcheck.diskcache")
expire = 24 * 60 * 60
md_headers = defaultdict(set)


def main(argv: t.Union[str, t.List[str], None] = None) -> None:
    """Main entrypoint."""
    argv = argv or sys.argv[1:]
    if isinstance(argv, str):
        argv = shlex.split(argv)
    args = parser.parse_args(argv)

    def ignore(url: str) -> bool:
        """Return True if the URL is ignored via any of the patterns."""
        return any(re.search(regex, url) for regex in args.ignore)

    with ThreadPoolExecutor() as executor:
        links = sorted(sum(executor.map(find_links, args.files), []))
        links = [link for link in links if not ignore(link.url)]
        checks = executor.map(check_link, links)
    errors = 0
    for link, status in zip(links, checks):
        if not status.success:
            print(f"{red}FAIL{reset} {link} - {status.message}")
            errors += 1
        elif debug:
            print(f"{green}PASS{reset} {link}")
    print(
        f"\n"
        f"Checked {bold}{q(len(links), 'link')}{reset} "
        f"in {bold}{q(len(args.files), 'file')}{reset} "
        f"({bold}{q(errors, 'error')}{reset})"
        f"\n"
    )
    if errors:
        sys.exit(1)


@dataclass(order=True)
class Link:
    """Link class with source filename, line no. and URL."""

    filename: str
    line_num: int
    url: str

    def __str__(self) -> str:
        """Return string representation of a link in a file."""
        return f"{self.filename}:{self.line_num} - {self.url}"


def find_links(filename: str) -> t.List[Link]:
    """Return list of links from a file."""
    with open(filename, "r", encoding="utf8") as file:
        try:
            lines = file.readlines()
        except UnicodeDecodeError:
            return []
    links = []
    for line_num, line in enumerate(lines, start=1):
        for url in link_re.findall(line):
            if not ignore_host(url):
                links.append(Link(filename, line_num, url.rstrip(".")))
        if not filename.lower().endswith(".md"):
            continue
        if md_header_re.match(line):
            md_headers[filename].add(md_header_ref(line))
        for url in md_link_re.findall(line):
            if not url.startswith("http"):
                links.append(Link(filename, line_num, url))
    return links


def ignore_host(url: str) -> bool:
    """Return True for URLs with dummy hostnames that shouldn't be checked."""
    try:
        host = urlparse(url).hostname
    except ValueError:
        return False
    if not host:
        return True
    if host in ignore_hosts:
        return True
    if host.split(".")[-1] in ignore_tlds:
        return True
    if "." not in host:
        return True
    return False


@dataclass
class Status:
    """Link-check result status with success and message."""

    success: bool
    message: t.Optional[str] = None

    @classmethod
    def ok(cls) -> "Status":
        """Return canonic successful check result instance."""
        return cls(success=True)

    @classmethod
    def error(cls, exc: Exception) -> "Status":
        """Return failed check result instance with exception/message."""
        return cls(success=False, message=str(exc).splitlines()[0])


def check_link(link: Link) -> Status:  # noqa PLR0912
    """Return link check result (with an error message on failure)."""
    url = ""
    hdr = {}
    kw = {"headers": hdr}
    get_kw = kw | {"stream": True, "raw": True}
    try:
        if link.url.startswith("http"):
            url = re.sub(r"#.*$", "", link.url)  # anchors
            url = re.sub(r".git@[0-9a-f]{6,40}$", "", link.url)  # reqs.txt git ref
            if url not in cache:
                if github_re.match(url) and github_token:
                    hdr["authorization"] = f"bearer {github_token}"
                http.head(url, **kw)
        elif link.filename.lower().endswith(".md"):
            if link.url.startswith("#"):
                ref = link.url.lstrip("#")
                assert ref in md_headers[link.filename], "invalid header ref"
            elif not mail_re.match(link.url):
                base_path = Path(link.filename).parent
                link_path = base_path / re.sub(r'#.*| +".*"', "", link.url)
                assert link_path.exists(), "invalid file ref"
    except (ClientError, ServerError) as exc:
        if is_helm_repo(url):
            pass  # helm chart repo - only serves /index.yaml
        elif exc.status_code in (401, 403):
            pass  # requires auth - consider ok
        elif exc.status_code == 429:
            pass  # has aggressive rate limiting (eg. medium) - consider ok
        elif exc.status_code in (404, 500, 501) and http.get(url, **get_kw):
            pass  # server only supports GET
        elif exc.status_code == 503 and url.startswith("https://gitlab.com"):
            pass  # gitlab private repo auth
        elif exc.status_code >= 500 and github_re.match(url) and not github_token:
            pass  # github has aggressive rate limiting and throws 5xx at random
        else:
            return Status.error(exc)
    except Exception as exc:
        return Status.error(exc)
    if url:
        cache.add(url, True, expire=expire)
    return Status.ok()


def is_helm_repo(url: str) -> bool:
    """Return True IFF the URL looks like a helm repo and serves /index.yaml."""
    return bool(helm_re.search(url) and http.get(f"{url}/index.yaml", stream=True))


def md_header_ref(line: str) -> str:
    """Return Markdown header ref as used for anchors."""
    ref = line.lstrip("#").strip().lower().replace(" ", "-")
    return re.sub(r"[^-a-z0-9_]", "", ref)


if __name__ == "__main__":
    main()
