#!/usr/bin/env bash
# pytest_local.sh - (executable) run pytest locally
set -Eeuo pipefail
DIR="$(cd "${0%/*}/.." && pwd)"
. "$DIR/scripts/utils.sh"

test -z "${DEBUG:-}" || set -x

PYPROJ_NAME=$(sed -En 's/^name = "(.*)"\s*/\1/p' pyproject.toml 2>/dev/null || :)
PYPROJ_NAME=${PYPROJ_NAME:-${PWD##*/}}


main() {
    pytest_cmd=(pytest "$@")
    # build and run within docker if a dockerfile is present
    if [[ -f Dockerfile ]]; then
        # use buildx if available
        docker buildx --help &>/dev/null \
            && build_cmd=(docker buildx build) \
            || build_cmd=(docker build)
        # key image name off of manifest for gears
        test -f manifest.json \
            && build_img=$(jq -r '.custom["gear-builder"].image' manifest.json)-test \
            || build_img=flywheel/$PYPROJ_NAME-test
        # use the dev target if there is one
        grep -iq '^FROM .* AS dev$' Dockerfile \
            && build_target=dev \
            || build_target=
        # build the image
        "${build_cmd[@]}" -t"$build_img" --target="$build_target" .
        # capture env into a file
        env_file=$(mktemp)
        trap 'rm "$env_file"' EXIT
        env | grep -E '^[A-Z][A-Z0-9_]*=.*' | grep -Ev '^PATH=' >"$env_file"
        # prepend the cmd w/ 'docker run'
        pytest_cmd=(
            docker run --rm -tu0:0 --privileged -v"$PWD:/test" -w/test
            --env-file="$env_file" --entrypoint=
            -eTERM=xterm -eCOLUMNS=154 -eLINES=35
            "$build_img" "${pytest_cmd[@]}"
        )
    fi
    # enable verbose pytest output when debugging
    test -n "${DEBUG:-}" && pytest_cmd+=(-vvv)
    # setup coverage reporting and failure threshold (default: 100)
    # deprecated - please use pytest addopts in pyproject.toml instead
    test_cov=$(grep PYTEST_COV_FAIL_UNDER .gitlab-ci.yml \
        | sed 's/#.*//' | grep -Eo '[0-9]+' | head -n1 || :)
    pytest_cmd+=(
        --cov="${PYPROJ_NAME//-/_}"
        --cov-fail-under="${test_cov:-100}"
        --cov-report=term-missing
        --cov-report=xml:coverage.xml
        --junitxml=junit.xml
        -o=junit_family=xunit2
    )
    # dispatch to pytest
    exec "${pytest_cmd[@]}"
}


main "$@"
