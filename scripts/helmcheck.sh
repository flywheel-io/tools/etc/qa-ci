#!/usr/bin/env bash
# helmcheck.sh - (executable) auto-document and test helm charts
set -Eeuo pipefail
USAGE="Usage: $0 [CHART]

Run a suite of tools on the given helm chart dir (default: ./helm/<proj>).
- Run 'helm-docs' to generate helm chart's README.md from values.yaml
- Run 'helm dependency update' to ensure chart deps are up-to-date
- Fix common errors and standardize skaffold.yaml if it exists
- For each test values file at ./helm/<proj>/tests/<test>.yaml, run:
    - Run 'helm template --values <test>.yaml' to render the manifests
    - Run 'helm lint --values <test>.yaml' to lint the chart
    - Run 'kubeconform' on every render to validate against the k8s schema
    - Run 'kubesec' on every render to list vulnerabilities
    - Run 'yamllint' on every render for formatting consistency

Test values are useful for exercising conditional logic in the templates
and enabling the rendering of charts with explicitly required values.
If skaffold.yaml exists, its setValues are used as test values.
If there are no tests, the chart will be tested using defaults.
"
DIR="$(cd "${0%/*}/.." && pwd)"
. "$DIR/scripts/utils.sh"
test -z "${DEBUG:-}" || set -x


main() {
    echo "$*" | grep -Eqvw "help|--help|-h" || { echo "$USAGE"; exit; }

    test "$#" -gt 0 || set -- "${HELM_DIR:-helm/$CI_PROJECT_NAME}"
    CHART="$1"; NAME="$(basename "$CHART")"
    ROOT="$PWD"
    SKAFF="$ROOT/skaffold.yaml"
    CACHE="$ROOT/.cache/kubeconform"
    mkdir -p "$CACHE"

    log "Checking helm chart $CHART"
    cd "$CHART" || die "Cannot cd into $CHART"

    log "Running helm-docs"
    helm-docs --sort-values-order file

    log "Running helm dependency build"
    helm_init
    helm dependency build || :

    if [[ -f "$SKAFF" ]]; then
        log "Checking skaffold.yaml"
        yq_skaff() { yq -i "$@" "$SKAFF"; }
        # manually bump to a version with the manifests field
        SCHEMA_VER=$(yq -r .apiVersion "$SKAFF" | sed -E 's|skaffold/v||;s|beta|.|')
        vergte "$SCHEMA_VER" 4.5 || yq_skaff '.apiVersion="skaffold/v4beta5"'
        # enable fast and efficient local image build via docker/buildkit
        yq_skaff ".build.local.useBuildkit=true"
        yq_skaff ".build.local.useDockerCLI=true"
        # use standardized project-based image name (and dev target, if available)
        yq_skaff ".build.artifacts[0].image=\"flywheel/$NAME\""
        if grep -iq "^FROM .* AS dev$" "$ROOT/Dockerfile"; then
            yq_skaff ".build.artifacts[0].docker.target=\"dev\""
        fi
        # move deploy -> manifests as part of the skaffold v2 migration
        # https://skaffold.dev/docs/renderers/helm/#configuring-your-helm-project-with-skaffold
        sed -Ei "s/^deploy:$/manifests:/" "$SKAFF"
        sed -Ei "s/^( +)overrides:$/\1setValues:/" "$SKAFF"
        # use standardized project-based release name and chart path
        REL0=".manifests.helm.releases[0]"
        yq_skaff "$REL0.name=\"flywheel-${NAME#flywheel-}\""
        yq_skaff "$REL0.chartPath=\"helm/$NAME\""
        # remove old image injection mode: imageStrategy/arifactOverrides
        yq_skaff "del($REL0.imageStrategy)"
        yq_skaff "del($REL0.artifactOverrides)"
        # insert new image injection mode: setValueTemplates
        IMAGE="flywheel_${NAME//-/_}"
        yq_skaff "$REL0.setValueTemplates.\"image.repository\"=\"{{.IMAGE_REPO_$IMAGE}}\""
        yq_skaff "$REL0.setValueTemplates.\"image.tag\"=\"{{.IMAGE_TAG_$IMAGE}}@{{.IMAGE_DIGEST_$IMAGE}}\""
        # move/flatten overrides -> setValues (overrides don't work as expected in v2)
        SETVALS=/tmp/$NAME-skaffold-setvalues.yml
        yq "$REL0.setValues" "$SKAFF" \
            | yq '..|select(type!="!!map" and type!="!!seq")|{path|join("."):.}' \
            >"$SETVALS"
        yq_skaff "$REL0.setValues={}"
        for KEY in $(yq -r 'keys[]' "$SETVALS"); do
            yq_skaff "$REL0.setValues.\"$KEY\"=$(yq -oj ".\"$KEY\"" "$SETVALS")"
        done
        skaffold schema get "$(yq -r .apiVersion "$SKAFF")" | \
        check-jsonschema --schemafile - "$SKAFF"
    fi

    log "Collecting test values YAMLs"
    # TODO seems to trip if no tests present
    mapfile -t TESTS < <(find . -type f | grep -v render | grep -E "test.*\.ya?ml" | sort || true)
    if [[ -f "$SKAFF" ]]; then
        log "Adding skaffold value render test"
        echo "{}" >/tmp/test_skaffold.yaml
        for KEY in $(yq "$REL0.setValues|keys|.[]" "$SKAFF"); do
            VAL="$(yq -ojson "$REL0.setValues.\"$KEY\"" "$SKAFF")"
            yq -i ".$KEY=$VAL" /tmp/test_skaffold.yaml
        done
        TESTS+=(/tmp/test_skaffold.yaml)
    fi
    if [[ -z "${TESTS[*]}" ]]; then
        log "No helm test values found, adding default render test"
        echo "{}" >/tmp/test_defaults.yaml
        TESTS+=(/tmp/test_defaults.yaml)
    fi

    mkdir -p render
    test -s render/.gitignore || echo "*" >render/.gitignore
    # select yamllint config (helm rc >> project rc >> qa-ci default rc)
    for YAMLLINT_CONF in .yamllint.yml ../../.yamllint.yml /qa-ci/.yamllint.yml; do
        ! test -f "$YAMLLINT_CONF" || break
    done
    # pass a looser config by default due to subcharts
    YAMLLINT_CONF_HELM=$(yq -I0 -oj \
        '(.rules.indentation.level="warning")
        |(.rules.line-length.level="warning")
        |(.rules.trailing-spaces.level="warning")' "$YAMLLINT_CONF")

    for TEST in "${TESTS[@]}"; do
        log "Testing chart with --values=$TEST"
        helm lint . --strict --values="$TEST"
        for V_K8S in ${K8S_VERSION//,/ }; do
            RENDER="render/${V_K8S}_$(basename "$TEST")"
            log "Rendering chart in $RENDER"
            helm template flywheel . --debug --kube-version="${V_K8S#v}" --values="$TEST" >"$RENDER"
            yamllint -d "$YAMLLINT_CONF_HELM" -f colored "$RENDER"
            kubeconform -kubernetes-version="${V_K8S#v}" \
                -schema-location=default \
                -schema-location="https://raw.githubusercontent.com/datreeio/CRDs-catalog/main/{{.Group}}/{{.ResourceKind}}_{{.ResourceAPIVersion}}.json" \
                -strict -summary -cache="$CACHE" "$RENDER"
            kubesec_scan --kubernetes-version="${V_K8S#v}" "$RENDER"
        done
    done
}


kubesec_scan() {
    section_start kubesec_scan
    SCAN="/tmp/kubesec_scan.json"
    SEEN="/tmp/kubesec_seen_$$.json"
    test -f "$SEEN" || echo "{}" >"$SEEN"
    # kubesec is not enforced and doesn't fail pipelines (yet)
    # set KUBESEC_STRICT=true to start enforcing
    kubesec scan "$@" >"$SCAN" || test -z "${KUBESEC_STRICT:-}" && STATUS=0 || STATUS=1
    ! grep -i "invalid input" "$SCAN" || return "$STATUS"
    # format results as yaml and only report the same rule once per object
    python <<EOF | yq
import json
scan = json.load(open("$SCAN"))
seen = json.load(open("$SEEN"))
for elem in scan:
    if elem['message'] == "This resource kind is not supported by kubesec":
        continue
    print(f"{elem['fileName']}:{elem['object']}:")
    print(f"  score: {elem['score']}")
    first = True
    for advise in elem.get('scoring', {}).get('advise', []):
        key = f"{elem['object']}/{advise['id']}"
        if key in seen:
            continue
        if first:
            print(f"  advise:")
            first = False
        print(f"    {advise['id']}: {advise['selector']}")
        seen[key] = True
json.dump(seen, open("$SEEN", "w"))
EOF
    section_end kubesec_scan
    return "$STATUS"
}


main "$@"
