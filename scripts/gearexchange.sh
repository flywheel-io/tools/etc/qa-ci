#!/usr/bin/env bash
# gearexchange.sh - (executable) open exchange mr when releasing a gear
set -Eeuo pipefail
USAGE="Usage: $0 [options]

Create an MR in the gear-exchange repo to update the current gear's version.
Fails if the gear version in the manifest is already up-to-date in exchange.
"
DIR="$(cd "${0%/*}/.." && pwd)"
. "$DIR/scripts/utils.sh"
. "$DIR/scripts/jobs.sh"
test -z "${DEBUG:-}" || set -x

GEAR_EXCHANGE_REPO=flywheel-io/scientific-solutions/gears/gear-exchange


main() {
    echo "$*" | grep -Eqvw "help|--help|-h" || { echo "$USAGE"; exit; }

    manifest_version() { jq -r .version "${1:-manifest.json}" | sed 's/_.*//'; }
    GEAR_NAME="$(jq -r .name manifest.json)"
    GEAR_IMG="$(jq -r '.custom."gear-builder".image' manifest.json)"
    GEAR_VERSION="${CI_COMMIT_TAG:-$(manifest_version)}"

    log "Cloning gear-exchange and updating the manifest for $GEAR_NAME"
    git_login
    git_clone "$GEAR_EXCHANGE_REPO"
    EXCHANGE_MANIFEST="gears/${GEAR_IMG%%/*}/$GEAR_NAME.json"
    if [[ ! -f "$EXCHANGE_MANIFEST" ]]; then
        log "Creating new gear-exchange manifest for $GEAR_NAME $GEAR_VERSION"
    elif [[ "$GEAR_VERSION" != "$(manifest_version "$EXCHANGE_MANIFEST")" ]]; then
        log "Updating old gear-exchange manifest to $GEAR_NAME $GEAR_VERSION"
    else
        die "$GEAR_NAME is already at version $GEAR_VERSION in gear-exchange"
    fi
    cp "$CI_PROJECT_DIR/manifest.json" "$EXCHANGE_MANIFEST"
    git add "$EXCHANGE_MANIFEST"
    log "Creating gear-exchange MR for updating $GEAR_NAME to $GEAR_VERSION"
    CI_PROJECT_ID="$GEAR_EXCHANGE_REPO" \
    git_mr "update-$GEAR_NAME-version-to-$GEAR_VERSION" master title="$GEAR_IMG" \
        description="Update $GEAR_NAME version to $GEAR_VERSION from $EXCHANGE_MANIFEST"
}


main "$@"
