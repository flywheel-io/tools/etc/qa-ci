#!/usr/bin/env bash
# gearcheck.sh - (executable) screen/fix common problems in flywheel gears
set -Eeuo pipefail
USAGE="Usage: $0

Lint Flywheel gears, checking for expected files/versions and common mistakes.

manifest.json   Ensure that the file exists and is a valid gear manifest.
pyproject.toml  Ensure that the file exists and that
                - the package directory is 'fw_gear_<GEAR_NAME_FROM_MANIFEST>'
                - the package name is 'fw-gear-<GEAR_NAME_FROM_MANIFEST>'
                - the package version is '<GEAR_VERSION_FROM_MANIFEST>'
.dockerignore   Ensure that the file exists and that
                - it starts with excluding everything using **
                - it re-includes the package directory
"
DIR="$(cd "${0%/*}/.." && pwd)"
. "$DIR/scripts/utils.sh"
test -z "${DEBUG:-}" || set -x


main() {
    echo "$*" | grep -Eqvw "help|--help|-h" || { echo "$USAGE"; exit; }
    validate_manifest
    validate_pyproject
    validate_dockerignore
}


validate_manifest() {
    log "gearcheck: manifest.json"
    test -f manifest.json || die "not found: manifest.json"
    validate_classification=$(grep -Ei 'VALIDATE_CLASSIFICATION\s*:' .gitlab-ci.yml \
    | sed -E 's/#.*//' | grep -Eoi '(true|false)' | head -n1 | tr '[:upper:]' '[:lower:]' || :)
    # check var presence
    # Check if VALIDATE_CLASSIFICATION is set and validate it
    if [[ -z "${validate_classification:-}" ]]; then
        warn "VALIDATE_CLASSIFICATION not set, skipping classification validation"
        validate_classification="false"
    else
        # Ensure case-insensitive comparison
        log "Validate classification set to $validate_classification"
    fi

    # Pin specific version of fw-gear for validation
    REQUIRED_VERSION="0.2.1"
    # Ensure fw-gear is installed with error handling
    if pip show fw-gear &>/dev/null; then
    INSTALLED_VERSION=$(pip show fw-gear | awk '/Version/ {print $2}')
    if [[ "$INSTALLED_VERSION" != "$REQUIRED_VERSION" ]]; then
        pip install --upgrade fw-gear==$REQUIRED_VERSION &>/dev/null || \
        die "Failed to install fw-gear version $REQUIRED_VERSION"
    fi
    else
    pip install fw-gear==$REQUIRED_VERSION &>/dev/null || \
        die "Failed to install fw-gear version $REQUIRED_VERSION"
    fi
    # TODO allow debugging install failures
    pip show fw-gear &>/dev/null || pip install "fw-gear[sdk]" >/dev/null
    python <<EOF || die "invalid manifest - see error above"
import sys
import os
from fw_gear.manifest import Manifest, ManifestValidationError
validate_classification = "$validate_classification" == "true"

try:
    manifest = Manifest("manifest.json")
    manifest.validate(validate_classification=validate_classification)
    if err := manifest._is_docker_names_match():
        raise ValueError(f"Docker name mismatch: {err}. Please check")
except (ManifestValidationError, ValueError) as exc:
    print(f"{type(exc).__name__}: {exc}", file=sys.stderr)
    sys.exit(1)
EOF
}


validate_pyproject() {
    log "gearcheck: pyproject.toml"
    GEAR_NAME="$(jq -r .name manifest.json)"
    GEAR_VERSION="$(jq -r .version manifest.json | sed 's/_.*//')"
    GEAR_PKG="fw-gear-${GEAR_NAME//_/-}"  # the expected package name (dashes)
    PY_NAME="${GEAR_PKG//-/_}"  # the expected python package import name (underscores)
    if [[ ! -f pyproject.toml ]]; then
        # TODO consider auto-adding dependencies (Dockerfile? reqs?)
        warn "not found: pyproject.toml"
        log "generating new pyproject.toml with poetry"
        poetry init --no-interaction \
            --name="$GEAR_PKG" \
            --author="Flywheel <support@flywheel.io>"
        git add pyproject.toml  # ensure it's tracked
        log "please add project dependencies with 'poetry add <dep>'"
    fi
    if [[ "$PYPROJ_NAME" != "$GEAR_PKG" ]]; then
        warn "found: $PYPROJ_NAME in pyproject.toml, expecting $GEAR_PKG instead"
    fi
    if [[ "$PYPROJ_VER" != "$GEAR_VERSION" ]]; then
        warn "found $PYPROJ_VER in pyproject.toml, expecting $GEAR_VERSION instead"
    fi
    # TODO consider standalone hook if devs keep tripping on py packaging
    if ! [[ -f "$PY_NAME/__init__.py" ]]; then
        list_packages() { find . -mindepth 2 -maxdepth 2 -type f -name "__init__.py" \
            | grep -v tests | sed -E 's|./(.*)/__init__.py|\1|'; }
        mapfile -t CANDIDATES < <(list_packages)
        # if there is only one package exists in repo
        if [[ "${#CANDIDATES[@]}" == 1 ]]; then
            OLD_NAME="${CANDIDATES[0]}"
            warn "Found $OLD_NAME as package name, expected $PY_NAME instead. Please change if this is not intended."
        else
            # if 0 or more than 1 packages found, let users handle it
            warn "Found ${#CANDIDATES[@]} potential packages (" "${CANDIDATES[@]}" "), but no match for expected package name: $PY_NAME.
            Please make sure that $PY_NAME/__init__.py exists"
        fi
    fi
}


validate_dockerignore() {
    log "gearcheck: .dockerignore"
    # TODO consider standalone hook for standardizing dockerignore everywhere
    # TODO consider auto-generating dockerignore instead of failing
    test -f .dockerignore || die "not found: .dockerignore"
    test "$(grep -E '^[^#]+.*' .dockerignore | head -n1)" = "**" \
        || die ".dockerignore doesn't start with ${C_YELLOW}**${C_RESET}"
    PY_NAME="${PYPROJ_NAME//-/_}"
    grep -Eq "^!$PY_NAME" .dockerignore \
        || die ".dockerignore doesn't contain ${C_YELLOW}!$PY_NAME${C_RESET}"
    # TODO test for the other bits like pyproject, README, __pycache__, etc.

}


main "$@"
