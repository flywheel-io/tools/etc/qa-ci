#!/usr/bin/env bash
# utils.sh - (sourceable) utility/helper functions
# shellcheck disable=SC2034,SC2317
set -a
# qa-ci dir (ie. /qa-ci in a container or the path to the local pre-commit clone)
DIR="$(cd "${BASH_SOURCE%/*}/.." && pwd)"
# current project dir (eg. ~/flywheel-io/product/backend/core-api)
PWD="$(pwd)"
# xtrace prompt prefix for displaying bash script/line/function
PS4='+ ${BASH_SOURCE//$PWD\//}:${LINENO} ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
DEBUG="${DEBUG:-}"

# docker_run (aka. drun) config
export APK_INSTALL="${APK_INSTALL:-}"
export APT_INSTALL="${APT_INSTALL:-}"
export PIP_INSTALL="${PIP_INSTALL:-}"
export VENV_INSTALL="${VENV_INSTALL:-}"

# caching variables
CACHE="$PWD/.cache"
APK_CACHE_DIR="$CACHE/apk"
APT_CACHE_DIR="$CACHE/apt"
MYPY_CACHE_DIR="$CACHE/mypy"
PIP_CACHE_DIR="$CACHE/pip"
PIP_TOOLS_CACHE_DIR="$PIP_CACHE_DIR"
POETRY_CACHE_DIR="$CACHE/poetry"
PRE_COMMIT_HOME="$CACHE/pre-commit"
RUFF_CACHE_DIR="$CACHE/ruff"

# common regexes (for use with 'grep -E' and 'sed -E')
D="[0-9]"
RE_NUM="[1-9]$D*"
RE_HASH="[0-9a-f]{8,}\$"
RE_IMAGE="[[:alnum:]]+[[:alnum:]/:.-]+[[:alnum:]]"
RE_JIRA="(APPSEC|CEN|EX|FLYW|GEAR|PLA?T|VDS)-$RE_NUM"
RE_V_MAJOR="$D+"
RE_V_MINOR="$D+\.$D+"
RE_V_PATCH="$D+\.$D+\.$D+"
RE_V="v?($D+)(\.($D+)(\.($D+))?)?"  # 3rd party ver
RE_VERSION="${RE_VERSION:-$RE_V(_$RE_V)?(-rc\.$D+)?}"  # repo ver (dual 4 gears)
RE_PREVTAG="${RE_PREVTAG:-$RE_VERSION}"  # for matching last tag

# define color/control codes
C_BOLD=$'\033[1m'
C_RED=$'\033[31m'
C_GREEN=$'\033[32m'
C_YELLOW=$'\033[33m'
C_CYAN=$'\033[36m'
C_RESET=$'\033[0m'
set +a


# logging helpers with colorized levels, printing to sdterr (+x for debug)
log()  {( set +x; printf %b "${C_BOLD}${C_GREEN}INFO${C_RESET} $*\n" >&2; )}
warn() {( set +x; printf %b "${C_BOLD}${C_YELLOW}WARN${C_RESET} $*\n" >&2; )}
err()  {( set +x; printf %b "${C_BOLD}${C_RED}ERRO${C_RESET} $*\n" >&2; )}
die()  {( set +x; printf %b "${C_BOLD}${C_RED}CRIT${C_RESET} $*\n" >&2; ); exit 1; }
die_retry() {( set +x; printf %b "${C_BOLD}${C_RED}CRIT${C_RESET} $*\n" >&2; ); exit 75; }
# log section helpers for auto-collapsing parts of the output in gitlab-ci
# https://docs.gitlab.com/ee/ci/jobs/index.html#custom-collapsible-sections
section_start() {( set +x; echo -e "\033[0Ksection_start:$(date +%s):$1[collapsed=true]\r\033[0K${C_BOLD}${C_CYAN}${2:-$1}${C_RESET}"; )}
section_end()   {( set +x; echo -e "\033[0Ksection_end:$(date +%s):$1\r\033[0K"; )}
# export envvars from the top-level .gitlab-ci.yml variables
load_ci_vars() {
    set -a +u
    # shellcheck disable=SC1090
    . <(yq -oj '.variables // {}' .gitlab-ci.yml | \
        jq -r 'to_entries | map("\(.key)=\"\(.value|tostring)\"") | .[]')
    set -u +a
}
# simple exit handler (de-)registration
init_exit_handlers() {
    trap 'eval "${ON_EXIT:-true}"' EXIT
    init_exit_handlers() { true; }
}
add_exit_handler() { init_exit_handlers; ON_EXIT+="$1;"; }
del_exit_handler() { ON_EXIT="${ON_EXIT/$1;/}"; }
# string manipulation utilities
upper() { echo "${1^^}"; }
lower() { echo "${1,,}"; }
urlquote() { echo "${1//\//%2F}"; }
strip() { sed -E 's/^([[:space:]]*)//;s/([[:space:]]*)$//'; }
sort_len() { awk '{print length,$0}' | sort -nrsu | cut -d' ' -f2; }
get_section() {
    lines=$(grep -En "$1" "$2" | grep -Eo "^[0-9]+" | head -n2)
    read -rd'\n' begin end <<<"$lines" || :
    sed -n "$((begin+1)),$((end-1))p" "$2"
}
inc_ver() { echo "${1:-cat}" | sed -E "s/^($RE_V_PATCH).*/\1/" | awk -F. -vOFS=. '{$NF+=1; print}'; }
vsort() { sed '/-/!{s/$/_/}' | sort -rV | sed 's/_$//'; }
vergte() { [[ "$1" = "$(echo -e "$1\n$2" | vsort | head -n1)" ]]; }
vergt() { [[ "$1" != "$2" ]] && vergte "$1" "$2"; }
verlte() { vergte "$2" "$1"; }
verlt() { vergt "$2" "$1"; }
# json in-place manipulation helper
jqi() { jq "${@:2}" "$1" | sponge "$1"; }
# return the age of the given file in seconds
fileage() {( set +x; echo "$(( $(date "+%s") - $(date -r "$1" "+%s") ))"; )}
# return md5 content hash for a list of files/dirs (32-char)
filehash() {(
    set +x
    test "$#" -gt 0 || die "Argument required - no files to hash"
    FILES=()
    for FILE in $(printf "%s\n" "$@" | sort); do
        if   [[ -f "$FILE" ]]; then FILES+=("$FILE")
        elif [[ -d "$FILE" ]]; then mapfile -t FIND < <(find "$FILE" -type f | sort)
                                    FILES+=("${FIND[@]}")
        else die "Cannot hash $FILE: not a file or directory"
        fi
    done
    md5sum "${FILES[@]}" | md5sum | head -c32 && echo
)}

# cache output from arbitrary commands (use EXPIRE=N (sec) to limit reuse)
cache() {(
    set +x
    cache_setup
    CACHED="$CACHE/$1.$(echo "$*" | md5sum | cut -d' ' -f1).out"
    if [[ -f "$CACHED" && -n "${EXPIRE:-}" && "$(fileage "$CACHED")" -ge "$EXPIRE" ]]; then
        # clear any expired cache first
        rm "$CACHED"
    fi
    if [[ -f "$CACHED" ]]; then
        # if it's cached, print the saved output
        cat "$CACHED"
    else
        # otherwise run it and save to cache
        "$@" | tee "$CACHED" || { STATUS=$?; rm "$CACHED"; return "$STATUS"; }
    fi
)}
cache_setup() {(
    set +x
    test -f "/tmp/cache_setup_${CI_JOB_ID:-$$}" && return
    test -z "${CACHE_CLEAR:-}" || rm -rf "$CACHE"
    mkdir -p "$PIP_CACHE_DIR" "$POETRY_CACHE_DIR"
    echo "*" >"$CACHE/.gitignore"
    test "$(id -u)" != 0 || chown -R 0:0 "$CACHE"
    touch "/tmp/cache_setup_${CI_JOB_ID:-$$}"
)}
# retry arbitrary commands
retry() {(
    set +x
    ATTEMPT=0
    RETRIES="${RETRIES:-3}"
    until "$@"; do
        STATUS=$?
        ATTEMPT="$((ATTEMPT + 1))"
        test "$ATTEMPT" -lt "$RETRIES" && sleep 1 || return "$STATUS"
    done
)}
# print default branch name
git_default_branch() {(
    if [[ -f .git/refs/remotes/origin/HEAD ]]; then
        sed -E 's|.*/||' .git/refs/remotes/origin/HEAD
    elif [[ -f .git/refs/heads/master ]]; then echo master;
    elif [[ -f .git/refs/heads/main   ]]; then echo main;
    else echo main; fi  # guess is off on shallow clones w/ master
)}

# print git repo url (for project $1 - with auth/private repo access in ci)
# NOTE used in autoupdate but only works w/ public repos locally
# TODO consider bind-mounting the SSH keys on local pre-commit runs
git_url() {
    URL="$1"
    [[ "$URL" =~ ^http ]] || URL="https://gitlab.com/$URL"
    [[ "$URL" =~ .git$ ]] || URL="$URL.git"
    ! "$CI" || AUTH="$CI_PUSH_USER_NAME:$CI_PUSH_TOKEN@"
    echo "$URL" | sed -E "s|://|://${AUTH:-}|"
}
# clone an arbitrary gitlab repo into /tmp and cd into it
git_clone() {
    REPO="$1" && shift
    CLONE="/tmp/$REPO"
    set -- "$@" -q --depth 1 "$(git_url "$REPO")" "$CLONE"
    test -d "$CLONE" || git clone "$@"
    cd "$CLONE" || die "Cannot cd into $CLONE"
}
# print changelog based on git revs between [ $1:=lastTag .. $2:=HEAD ]
git_changelog() {
    REV1="${1:-${LAST_VER:-$(git rev-list --max-parents=0 HEAD | head -c8)}}"
    REV2="${2:-HEAD}"
    git cat-file -e "$REV1"  # fail-fast if rev1 not found (history too shallow?)
    RE_JIRA_PREFIX="^([^[:alnum:]]*(FLYW|GEAR)-[0-9]+[^[:alnum:]]*)*"
    # gather all the commits that hit this branch between rev1 and rev2
    mapfile -t REVS < <(git log --first-parent --format=%h --abbrev=8 "$REV1..$REV2")
    UPDATES=()
    for REV in "${REVS[@]}"; do
        show() { git show --no-patch --format="$1" "$REV"; }
        TITLE="$(show %s)"
        # scrape all the jira tickets as flexibly as possible
        mapfile -t ISSUES < <(show %B | grep -Eio "$RE_JIRA" \
            | tr "[:lower:]" "[:upper:]" | sort -uV)
        if show %b | grep -q "merge request"; then
            # omit auto-updates from the changelog and report them aggregated
            if echo "$TITLE" | grep -Eq "Merge branch 'update-deps'"; then
                UPDATES+=("$REV"); continue
            # use the mr title (instead of the generated "Merge branch <blah>")
            else
                TITLE="$(show %b | head -n1)"
            fi
        fi
        # strip any hand-crafted issue prefixes and ending period
        TITLE="$(echo "$TITLE" | sed -E "s/$RE_JIRA_PREFIX//;s/\.\$//")"
        # add changelog entry formatted as "d34db33f FLYW-456 Title"
        # NOTE gitlab renders the commit and the issues as links
        echo "- $REV ${ISSUES[*]} ${TITLE^}" | sed -E "s/ +/ /g"
    done
    # append any update commits in a single, final entry "Updates: sha1 sha2"
    test "${#UPDATES[@]}" = 0 || echo "- Updates: ${UPDATES[*]}"
}
# add all helm repos used across fw
helm_init() {
    ! test -f /tmp/helm_init || return 0  # run only once per job
    section_start helm_init "Adding helm repositories"
    HELM_REPOS=(
        bitnami               https://charts.bitnami.com/bitnami
        bitnami-archive       https://raw.githubusercontent.com/bitnami/charts/archive-full-index/bitnami
        elastic               https://helm.elastic.co
        flywheel              https://helm.dev.flywheel.io
        prometheus-community  https://prometheus-community.github.io/helm-charts
        stable                https://charts.helm.sh/stable
    )
    parallel --will-cite -j2 -n2 helm repo add ::: "${HELM_REPOS[@]}"
    section_end helm_init
    touch /tmp/helm_init
}

docker_build() {
    log "Building $DOCKER_FILE as $DOCKER_IMAGE"
    log "Setting image build context for $DOCKER_FILE"
    cp "$DOCKER_FILE" "$DOCKER_FILE.bkp"
    cat >>"$DOCKER_FILE" <<EOF
ENV BUILD_TIME=$(date --utc +%FT%TZ) \\
    COMMIT_REF=${COMMIT_REF:-} \\
    COMMIT_SHA=${COMMIT_SHA:-}
EOF
    # use 'docker buildx build' locally
    if ! "$CI"; then
        DOCKER_BUILD=(docker buildx build -t"$DOCKER_IMAGE")
    # use 'podman build' with the .podman ci template (default)
    elif [[ "${DOCKER_BIN:-}" = podman ]]; then
        DOCKER_BUILD=(podman build -t"$DOCKER_IMAGE" --jobs="$(($(nproc) + 1))")
        # opt-in layer caching to gitlab via DOCKER_BUILD_CACHE=true
        if [[ "${DOCKER_BUILD_CACHE:-}" =~ 1|true ]]; then
            test "${CACHE_CLEAR:-}" = true || \
            DOCKER_BUILD+=(--cache-from="$DOCKER_BUILD_IMAGE/cache")
            DOCKER_BUILD+=(--cache-to="$DOCKER_BUILD_IMAGE/cache")
        fi
    # use 'docker buildx build' with the .docker ci template (opt-in)
    elif [[ "${DOCKER_BIN:-}" = docker ]]; then
        # NOTE using multiarch & cache-to/from requires a driver other than docker
        # but using a different driver leads to --load (slow + single arch)
        # and still couldn't get layer caching to function like podman...
        DOCKER_BUILD=(docker buildx build -t"$DOCKER_IMAGE")
    fi
    DOCKER_BUILD+=(-f"$DOCKER_FILE" --network=host)
    test -z "${DOCKER_BUILD_PLATFORM:-}" \
        || DOCKER_BUILD+=(--platform="${DOCKER_BUILD_PLATFORM/,*}")
    test -z "${DOCKER_BUILD_SSH_AGENT:-}${SSH_AUTH_SOCK:-}" \
        || DOCKER_BUILD+=(--ssh=default)
    test -z "${DOCKER_TARGET:-}" \
        || DOCKER_BUILD+=(--target="$DOCKER_TARGET")
    if [[ -n "${DOCKER_BUILD_ARGS:-}" ]]; then
        mapfile -t ARGS < <(sed -E 's/ +/ /g;s/^ | $//;s/ /\n/g' <<<"$DOCKER_BUILD_ARGS")
        mapfile -t ARGS < <(printf -- "--build-arg=%s\n" "${ARGS[@]}")
        DOCKER_BUILD+=("${ARGS[@]}")
    fi
    if [[ -n "${DOCKER_BUILD_EXTRA_ARGS:-}" ]]; then
        mapfile -t ARGS < <(sed -E 's/ +/ /g;s/^ | $//;s/ /\n/g' <<<"$DOCKER_BUILD_EXTRA_ARGS")
        DOCKER_BUILD+=("${ARGS[@]}")
    fi
    if "$CI" && grep -q arm <<<"${DOCKER_BUILD_PLATFORM:-}"; then
        # ensure that the bin formats are fully registered for cross-building arm
        podman run --rm --privileged tonistiigi/binfmt --install all &>/dev/null
        podman run --rm --privileged multiarch/qemu-user-static --reset -p yes -c yes &>/dev/null
    fi
    DOCKER_BUILD+=("$@" "${DOCKER_BUILD_CONTEXT:-.}")
    STATUS=0
    if [[ -n "${DOCKER_IMAGE_DEV:-}" ]]; then
        log "Running ${C_CYAN}${DOCKER_BUILD[*]}${C_RESET} (dev target)"
        "${DOCKER_BUILD[@]}" -t"$DOCKER_IMAGE_DEV" --target=dev || STATUS=$?
    fi
    log "Running ${C_CYAN}${DOCKER_BUILD[*]}${C_RESET}"
    "${DOCKER_BUILD[@]}" || STATUS=$?
    mv "$DOCKER_FILE.bkp" "$DOCKER_FILE"
    test "$STATUS" = 0 || die "docker_build exited with $STATUS - expand logs above"
}

# return variant-specific tags
# TODO rework variant tagging (version >> variant vs. sha tags)
get_docker_tag() {
    if   [[ -z "${DOCKER_VARIANT:-}" ]]; then echo "${1:-latest}"
    elif [[ "${1:-latest}" = latest  ]]; then echo "$DOCKER_VARIANT"
    else                                      echo "$DOCKER_VARIANT-$1"
    fi
}

docker_run() {
    # if there's no dockerfile or DRUN_QA_CI=true, simply drun within qa-ci
    if [[ ! -f "$DOCKER_FILE" || "${DRUN_QA_CI:-}" = true ]]; then
        PIP_INSTALL=${PIP_INSTALL:-auto}
        /qa-ci/scripts/drun.sh "$@"
        return
    fi
    # otherwise use the project image to access all tools built within
    DOCKER_RUN=("${DOCKER_BIN:=docker}" run --rm -t --network=host --privileged)
    # bind-mount the qa-ci sources and set drun.sh as the entrypoint
    QA_CI_SRC="${HOST_DIR:-$DIR}"
    # ensure that the host path is under pwd when using ci w/ docker
    if [[ "$DOCKER_BIN" = docker ]]; then
        QA_CI_SRC="$CACHE/qa-ci"
        rm -rf "$QA_CI_SRC"
        cp -r /qa-ci "$QA_CI_SRC"
    fi
    DOCKER_RUN+=(-v"$QA_CI_SRC:/qa-ci" --entrypoint=/qa-ci/scripts/drun.sh)
    # bind-mount the project dir and set as the workdir (plus use root inside)
    DOCKER_RUN+=(-v"$PWD:$PWD" -w"$PWD" -u0:0)
    # correct tty size inside the container
    DOCKER_RUN+=(-eTERM="$TERM" -eCOLUMNS="$COLUMNS" -eLINES="$LINES")
    # enable using the host docker within the container
    DOCKER_RUN+=(-v/var/run/docker.sock:/var/run/docker.sock)
    # pass in registry credentials to the container via pwd (linux only)
    if [[ -d ~/.docker ]]; then
        mkdir -p .drun && echo "*" >.drun/.gitignore && rm -rf .drun/.docker
        cp -rf ~/.docker .drun/.docker
    fi
    # pass in ssh key via bind-mount if it's set and points to an existing file
    # NOTE for ci/dind, the file must be under pwd for the bind to work
    if [[ -f "${DOCKER_RUN_SSH_KEY:-}" ]]; then
        mkdir -p .drun && echo "*" >.drun/.gitignore && rm -rf .drun/.ssh
        mkdir -p .drun/.ssh
        cp -f "$DOCKER_RUN_SSH_KEY" .drun/.ssh/id
    fi
    # use dev image if the dev target's available
    if [[ -n "${DOCKER_IMAGE_DEV:-}" ]]; then
        DRUN_IMAGE=$DOCKER_IMAGE_DEV
        VENV_INSTALL=${VENV_INSTALL:-false}
    else
        DRUN_IMAGE=$DOCKER_IMAGE
        PIP_INSTALL=${PIP_INSTALL:-auto}
    fi
    # inject envvars using an env file in pwd
    mkdir -p .tmp
    echo "*" >.tmp/.gitignore
    add_exit_handler "rm -rf '$PWD/.tmp'"
    env | grep -E '^[A-Z][A-Z0-9_]*=.*' | grep -Ev '^PATH=' >.tmp/.env
    DOCKER_RUN+=(--env-file="$PWD/.tmp/.env")
    # run the command in the container
    log "Running ${C_CYAN}${DOCKER_RUN[*]} $DRUN_IMAGE $*${C_RESET}"
    "${DOCKER_RUN[@]}" "$DRUN_IMAGE" "$@"
}
