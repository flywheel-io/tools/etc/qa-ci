#!/usr/bin/env bash
# poetry_export.sh - (executable) generate requirements.txt from pyproject.toml
set -Eeuo pipefail
DIR="$(cd "${0%/*}/.." && pwd)"
. "$DIR/scripts/utils.sh"
test -z "${DEBUG:-}" || set -x

cache_setup

# only keep requirements.txt if not empty, and track it stored
canonize(){ if [[ -s "$1" ]]; then sort -d "$1" | sponge "$1"; else rm -f "$1"; fi; }

# fix lock if it's not up-to-date (ie. pyproject changed)
poetry lock --check &>/dev/null || poetry lock
# export prod deps (TODO use 'uv pip compile' instead)
poetry export --without-hashes >requirements.txt
canonize requirements.txt

# export group/extra deps
mapfile -t WITH_DEV < <(sed -En "s/.*tool.poetry.group.([a-z]+).dependencies.*/--with=\1/p" pyproject.toml)
if grep -Eq tool.poetry.dev-dependencies pyproject.toml; then WITH_DEV+=(--with=dev); fi
poetry export --without-hashes --all-extras "${WITH_DEV[@]}" >requirements-dev.txt
# exclude prod deps to get dev-only deps
comm -13 <(sort requirements.txt) <(sort requirements-dev.txt) | sponge requirements-dev.txt
canonize requirements-dev.txt
