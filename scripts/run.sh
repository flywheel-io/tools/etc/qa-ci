#!/usr/bin/env bash
# run.sh - (executable) main qa-ci entrypoint for both local pre-commit and ci
set -Eeuo pipefail

# BOOTSTRAP: ensure that we're running in a qa-ci container (local/ci parity)
# qa-ci dir (ie. /qa-ci in a container or the path to the local pre-commit clone)
DIR="$(cd "${0%/*}/.." && pwd)"
# current project dir (eg. ~/flywheel-io/product/backend/core-api)
PWD="$(pwd)"
# xtrace prompt prefix for displaying bash script/line/function
PS4='+ ${BASH_SOURCE//$PWD\//}:${LINENO} ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
DEBUG="${DEBUG:-}"  # set to non-empty for xtrace (use ints for more verbosity)
# 1=trace job/hook | 2=trace docker run (local) | 3=trace sourcing libs
case "$DEBUG" in
    ''|0|no|false) DEBUG= ;;  # coerce empty/0/no/false as '' to NOT DEBUG
    4|5|6|7|8|9)   DEBUG=3;;  # coerce 4+ to the current MAX DEBUG LEVEL
    2|3)                  ;;  # leave 2/3 as-is to DEBUG AT SPECIFIED LEVEL
    *)             DEBUG=1;;  # coerce every other non-empty to BASIC DEBUG
esac
test "${DEBUG:-0}" -lt 2 || set -x  # DEBUG=2

# when running in ci
if [[ "${CI:-}" = true ]]; then
    # tty vars for well-aligned / color output
    export TERM=xterm
    export COLUMNS=150
    export LINES=45
    # create a podman socket
    test "${DOCKER_BIN:-}" = docker || test -S /var/run/docker.sock || \
    podman system service --time=0 unix:///var/run/docker.sock &>/tmp/podman.log &
# when running in local pre-commit, exec cmd in a qa-ci container
elif [[ "${QA_CI:-false}" = false ]]; then
    # first things first - provide friendly error if docker is missing
    die() {( set +x; printf "%s\n" "$*" >&2; ); exit 1; }
    which docker &>/dev/null || die "'docker' not found - please install it first"
    # second, the docker engine might be down (eg. disabled on macs due to cpu/mem)
    docker info >/dev/null || die "'docker info' failed - is docker running?"
    test "${DEBUG:-0}" -lt 3 || docker info  # capture in debug mode for os/arch/etc
    # third, not all docker versions are born equal - let's assert a minimum
    MINVER=23.0.2
    DOCKER_VER="$(docker info | sed -En "s/ Server Version: (.*)/\1/p")"
    vergte() {( set +x; [[ "$1" = "$2" || "$2" = "$(printf "%s\n" "$@"|sort -V|head -n1)" ]]; )}
    vergte "$DOCKER_VER" "$MINVER" || die "docker >=$MINVER required, got $DOCKER_VER"
    # ^ after establishing a usable docker, let's put together a run command...
    DOCKER_RUN=(docker run --rm -t --network=host)
    # bind-mount the project dir and set as the workdir
    DOCKER_RUN+=(-v"$PWD:$PWD" -w"$PWD")
    # enable using the host's docker within the container
    test -S /var/run/docker.sock && \
    DOCKER_RUN+=(-v/var/run/docker.sock:/var/run/docker.sock)
    # pass in ssh agent socket for `docker build --ssh=default`
    test -n "${SSH_AUTH_SOCK:-}" && \
    DOCKER_RUN+=(-v"$SSH_AUTH_SOCK:$SSH_AUTH_SOCK" -eSSH_AUTH_SOCK)
    # tty vars for well-aligned / color output
    export TERM=xterm
    export COLUMNS=${COLUMNS:-$(tput cols 2>/dev/null || echo 150)}
    export LINES=${LINES:-$(tput lines 2>/dev/null || echo 45)}
    DOCKER_RUN+=(-eTERM="$TERM" -eCOLUMNS="$COLUMNS" -eLINES="$LINES")
    # pass in the debug and cache_clear and any gitlab token envvars
    for VAR in DEBUG CACHE_CLEAR DOCKER_{USER,PASS} GITLAB_{USER,TOKEN}; do
        test -n "${!VAR:-}" && DOCKER_RUN+=(-e"$VAR")
    done
    # pass in registry credentials to the container (linux only)
    if [[ "$(uname -s)" = Linux && -f ~/.docker/config.json ]]; then
        DOCKER_RUN+=(-v"$HOME/.docker/config.json:/root/.docker/config.json")
    fi
    # determine the qa-ci image - allow envvar overrides
    IMG=${QA_CI_IMAGE:-flywheel/qa-ci:${PYVER:-3.13}-${QA_CI_REF:-main}}
    # build the image when self-testing qa-ci, otherwise always pull
    if [[ "${PWD##*/}" = qa-ci ]]; then
        docker build . -t"$IMG"
    else
        DOCKER_RUN+=(--pull="${PULL_POLICY:-always}")
    fi
    # let's roll, do the same thing but now within the container
    echo "Execing into $IMG" >&2
    exec "${DOCKER_RUN[@]}" "$IMG" /qa-ci/scripts/run.sh "$@"
fi

# TODO alpine apk install/cache support out of the box

# UTILITIES: source shared, hook and job functions
set +x
. "$DIR/scripts/utils.sh"
. "$DIR/scripts/hooks.sh"
. "$DIR/scripts/jobs.sh"

# ENVIRONMENT: establish a baseline of envvars (local/ci parity)
test "${DEBUG:-0}" -lt 2 || set -x  # DEBUG=2
# TODO reconsider allexport - turns out it doesn't always deliver...
set -a
CI="${CI:-false}"
QA_CI="${QA_CI:-false}"
QA_CI_SHA="${QA_CI_SHA:-${COMMIT_SHA:-}}"
CACHE_CLEAR="${CACHE_CLEAR:-}"
PWD_OWNER="$(stat -c%u:%g "$PWD")"
ON_EXIT=
# allow using common ci vars in local hook runs by defaulting them via git
"$CI" || git config --global --add safe.directory "$PWD"
CI_PROJECT_NAME="${CI_PROJECT_NAME:-$(basename "$PWD")}"
CI_PROJECT_PATH="${CI_PROJECT_PATH:-$(git remote get-url origin | sed -E 's#(git@|https://)([^/]+).(.*).git#\3#')}"
CI_DEFAULT_BRANCH="${CI_DEFAULT_BRANCH:-$(git_default_branch)}"
CI_COMMIT_BRANCH="${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME:=${CI_COMMIT_BRANCH:=$(git branch --show-current 2>/dev/null)}}"
CI_COMMIT_SHA="${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA:=${CI_COMMIT_SHA:=$(git rev-parse HEAD)}}"
CI_COMMIT_TAG="${CI_COMMIT_TAG:-}"  # git tag (if the current commit is a tag)
CI_API_V4_URL="${CI_API_V4_URL:-"https://gitlab.com/api/v4"}"
CI_JOB_TOKEN="${CI_JOB_TOKEN:-${GITLAB_TOKEN:-}}"
GITLAB_CI_BOT_TOKEN="${GITLAB_CI_BOT_TOKEN:-${GITLAB_TOKEN:-}}"
# extend the repo-related vars with some useful shorthands of our own
PYPROJ_NAME="$(sed -En 's/^name = "(.*)"\s*/\1/p' pyproject.toml 2>/dev/null || echo "$CI_PROJECT_NAME")"
PYPROJ_VER="$(sed -En 's/^version = "(.*)"\s*/\1/p' pyproject.toml 2>/dev/null || true)"
NPM_VER="$(jq -r '.version // ""' package.json 2>/dev/null || true)"

COMMIT_SHA="${CI_COMMIT_SHA:0:8}"  # git commit sha (8 char only)
COMMIT_REF="${CI_COMMIT_REF_NAME:=${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}}"
KEY_COMMIT="${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH.$COMMIT_SHA}"
# get most recent tagged version for
# - syncing project version in tracked files (eg. pyproject, helm chart, etc.)
# - and to derive semver-compatible pre-releases from (eg. for helm chart)
# use the first method that works (lack of git history might get in the way):
# - the most recent tag among parent commits
# - the version in pyproject.toml
LAST_VER="$(git tag --merged=HEAD --sort=-v:refname 2>/dev/null | grep -E "^$RE_PREVTAG\$" | head -n1 || true)"
LAST_VER="${LAST_VER:-${PYPROJ_VER:-}}"

test -z "$NPM_VER" || verlte "$NPM_VER" "$LAST_VER" || LAST_VER="$NPM_VER"
test -z "$PYPROJ_VER" || verlte "$PYPROJ_VER" "$LAST_VER" || LAST_VER="$PYPROJ_VER"

test -n "$LAST_VER" && NEXT_VER="$(inc_ver "$LAST_VER")" || NEXT_VER=0.1.0
test "$CI_PROJECT_NAME" = qa-ci && QA_CI_SHA="$COMMIT_SHA"
CONTEXT="ci:$CI qa-ci:$QA_CI_SHA ${CI_PROJECT_PATH#flywheel-io/}:$COMMIT_REF"
# CONFIGURATION: export qa-ci config vars
"$CI" || load_ci_vars  # load .gitlab-ci.yml variables on local pre-commit runs
DOCKER_FILE="${DOCKER_FILE:-Dockerfile}"
DOCKER_IMAGE="${DOCKER_IMAGE:-flywheel/$CI_PROJECT_NAME}"
if grep -iq ' AS dev$' "$DOCKER_FILE" 2>/dev/null; then
    DOCKER_IMAGE_DEV=$DOCKER_IMAGE/dev
fi
HELM_DIR="${HELM_DIR:-${HELM_CHART_DIR:-helm/$CI_PROJECT_NAME}}"
K8S_VERSION="${K8S_VERSION:-v1.24.12}"
FW_UMBRELLA="${FW_UMBRELLA:-flywheel-io/infrastructure/umbrella}"
FW_COMPONENT="${FW_COMPONENT:-}"
UPDATE_SKIP="${UPDATE_SKIP:-}"
UPDATE_QA_CI_REV="${UPDATE_QA_CI_REV:-}"
set +a

# CLEANUP: avoid clobbering local venvs (poetry invocations break .venv)
if [[ -d .venv ]]; then
    mv .venv .venv.bkp
    add_exit_handler "mv .venv.bkp .venv"
fi
# CLEANUP: restore file ownership on local runs
# TODO create user/group + docker group on pwd owner mismatch and gosu instead
test "$PWD_OWNER" = "0:0" || add_exit_handler "chown -R $PWD_OWNER $PWD"
# CACHE: initialize gitignored cache dir (local and ci use, 5GB limit in gitlab)
cache_setup
# QA_CI_CACHE: initialize custom cache stored in gs://qa-ci-cache (flywheel-dev)
if "$CI" && [[ "${QA_CI_CACHE:-}" = true ]]; then qa_ci_cache_setup; fi
# GCLOUD_INSTALL: auto-install the google cloud sdk for usage in jobs
if "$CI" && [[ "${GCLOUD_INSTALL:-}" = true ]]; then gcloud_install; fi

# RUN: finally dispatch the invoked command
test "${DEBUG:-0}" -lt 1 || set -x  # DEBUG=1
"$@"
