#!/usr/bin/env python
"""Fix line endings in text files.

- Replace CRLF and CR to LF
- Trim whitespace before LF
- Enforce one LF at the end
"""

import sys
from pathlib import Path
from typing import List, Union


def main(argv: Union[str, List[str], None] = None) -> int:
    """Run main entrypoint."""
    argv = argv or sys.argv[1:]
    if not argv:  # pragma: no cover
        sys.stdout.write(eolfix(sys.stdin.read()))
        return 0
    for file in argv:
        path = Path(file)
        path.write_text(eolfix(path.read_text()))
    return 0


def eolfix(text: str) -> str:
    """Return text with uniform LF line endings and without trailing spaces."""
    text = "\n".join(line.rstrip() for line in text.splitlines()).rstrip()
    return f"{text}\n" if text else ""


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
