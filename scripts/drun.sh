#!/usr/bin/env bash
# drun.sh - (executable) entrypoint for running commands in containers
# used for auto-installing lib dev-dependencies (eg. apk/apt/build or pip/dev)
# shellcheck disable=SC2086,SC2174
set -Eeuo pipefail
DIR="$(cd "${0%/*}/.." && pwd)"
. "$DIR/scripts/utils.sh"
test -z "${DEBUG:-}" || set -x

cache_setup
PWD_OWNER="$(stat -c%u:%g "$PWD")"
test "$PWD_OWNER" = 0:0 || trap 'find . -user root | xargs chown "$(stat -c%u:%g .)"' EXIT

# setup docker helper vars and creds within the container
export BUILD_HOST
test "${CI:-}" = true && BUILD_HOST=build || BUILD_HOST="${DOCKER_HOST:-localhost}"
if [[ -f .drun/.docker/config.json ]]; then
    mkdir -p ~/.docker
    cp .drun/.docker/config.json ~/.docker
fi
if [[ -f .drun/.ssh/id ]]; then
    mkdir -pm 0600 ~/.ssh
    cp -f .drun/.ssh/id ~/.ssh/id
    chmod 0400 ~/.ssh/id
    eval "$(ssh-agent -s)"
    ssh-add ~/.ssh/id
fi

# install any apk/apt packages listed in $APK_INSTALL / $APT_INSTALL
timer() { log "running $*"; time "$@" || die "cmd exited with $?"; }

apk_install() { timer apk add "$@"; }
if [[ -n "${APK_INSTALL:-}" ]] && which apk &>/dev/null; then
    mkdir -p "$APK_CACHE_DIR"
    rm -rf /etc/apk/cache
    ln -sf "$APK_CACHE_DIR" /etc/apk/cache
    apk_install ${APK_INSTALL//,/ }
fi

apt_install() { timer apt-get install -qy --no-install-recommends "$@"; }
if [[ -n "${APT_INSTALL:-}" ]] && which apt &>/dev/null; then
    rm -f /etc/apt/apt.conf.d/docker-clean
    cat >/etc/apt/apt.conf.d/qa-ci-cache <<EOF
dir::cache        $APT_CACHE_DIR;
dir::state::lists $APT_CACHE_DIR/lists;
EOF
    if [[ ! -d "$APT_CACHE_DIR" ]]; then
        mkdir -p "$APT_CACHE_DIR"/{archives,lists}/partial
        apt-get update
    fi
    apt_install ${APT_INSTALL//,/ }
fi

# install any python packages listed in $PIP_INSTALL (supports 'auto')
which uv &>/dev/null && PIP_CMD=(uv pip) || PIP_CMD=(pip)
pip_install() { timer "${PIP_CMD[@]}" install "$@"; }
if [[ -n "${PIP_INSTALL:-}" ]]; then
    # NOTE auto-detecting python version in the container
    # if there are multiple py installs, the desired one must be the 1st in PATH
    PYVER="$(python -V | sed -En "s/.*([0-9]+\.[0-9]+)\.[0-9]+/\1/p")"
    vergte "$PYVER" "3.12.0" || export SETUPTOOLS_USE_DISTUTILS=stdlib
    # create and use a venv in a bind-mounted / cache-able location
    # enabled by default unless using with a 'dev' docker target
    # can be explicitly disabled via VENV_INSTALL=false
    if [[ "${VENV_INSTALL:-true}" =~ 1|true ]]; then
        test "${QA_CI:-}" = true \
            && VIRTUAL_ENV="$CACHE/venv-qa-ci-$PYVER" \
            || VIRTUAL_ENV="$CACHE/venv-image-$PYVER"
        if [[ ! -d "$VIRTUAL_ENV" ]]; then
            python"$PYVER" -mvenv "$VIRTUAL_ENV"
            pip_install -U pip setuptools wheel
        fi
        export PATH="$VIRTUAL_ENV/bin:$PATH"
        export VIRTUAL_ENV
    fi
    # install dev-dependencies
    if [[ "$PIP_INSTALL" != auto ]]; then
        pip_install ${PIP_INSTALL//,/ }
    else
        # NOTE pyproject.toml extras supported IFF there's one named 'all'
        # NOTE requirements supported IFF named requirements[-dev].txt
        PIP_ARGS=()
        grep tool.poetry.extras pyproject.toml &>/dev/null && EXTRAS="[all]" || EXTRAS=""
        test -f pyproject.toml       && PIP_ARGS+=(-e".$EXTRAS")
        test -f requirements.txt     && PIP_ARGS+=(-rrequirements.txt)
        test -f requirements-dev.txt && PIP_ARGS+=(-rrequirements-dev.txt)
        pip_install "${PIP_ARGS[@]}"
    fi
fi

# auto-install docker and docker-compose binaries if pytest-docker is present
# this allows seamless integration testing using containers within pytest
if [[ "${1:-}" = pytest ]] && grep -Rq pytest-docker; then
    ARCH="$(uname -m)"
    mkdir -p "$CACHE/bin"
    export PATH=$PATH:$CACHE/bin
    if ! which docker &>/dev/null; then
        V_DOCKER=27.2.1
        xh -IF "https://download.docker.com/linux/static/stable/$ARCH/docker-$V_DOCKER.tgz" \
            | tar xz --strip-components=1 docker/docker
        chmod +x docker
        mv docker "$CACHE/bin"
    fi
    if ! which docker-compose &>/dev/null; then
        V_COMPOSE=v2.29.4
        xh -IFodocker-compose "https://github.com/docker/compose/releases/download/$V_COMPOSE/docker-compose-linux-$ARCH"
        chmod +x docker-compose
        mv docker-compose "$CACHE/bin"
    fi
fi

# run the command
"$@"
