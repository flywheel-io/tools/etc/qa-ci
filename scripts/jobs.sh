#!/usr/bin/env bash
# job.sh - (sourceable) gitlab-ci job functions
# shellcheck disable=SC2119

# wrapper for logging startup and adding troubleshooter
job() {(
    test "${DEBUG:-0}" -lt 3 || set -x  # DEBUG=3
    test "$#" -gt 0 || die "Command required"
    log "$1 $CONTEXT"
    log "Running $*"
    "$@" && STATUS=0 || STATUS=$?
    # TODO rewrite and re-add troubleshooter
    exit "$STATUS"
)}

lint_pre_commit() {
    # clear the pre-commit cache if any rev looks mutable (ie. not a tag/commit SHA)
    for REV in $(yq ".repos[].rev" .pre-commit-config.yaml); do
        echo "$REV" | grep -Eq "$RE_V|$RE_HASH" || { pre-commit clean; break; }
    done
    # cache was already cleared if set, unset to avoid re-wiping in each hook
    # TODO a great refact would be better; wrapping the entire pre-commit once
    export CACHE_CLEAR=
    export SKIP="${SKIP:+$SKIP,}docker_build,generate_docs,mypy,pytest"
    STATUS=
    pre-commit run --all-files --verbose --color always "$@" || STATUS=$?
    if [[ "${STATUS:=0}" != 0 ]] && has_diff; then
        log "Pre-commit hooks changed files - committing auto-fix"
        git_push
    fi
    ensure_artifact "$HELM_DIR"/render/
    exit "$STATUS"
}

build_docker() {
    set_version
    docker_login  # login early in case using a private image in FROM
    # remove any prev image in case running multiple builds per job
    podman image rm "$DOCKER_IMAGE" &>/dev/null || :
    VARIANT="${DOCKER_VARIANT:-default}"
    SECTION_DBUILD="docker_build_$VARIANT"
    section_start "$SECTION_DBUILD" "Building image $DOCKER_IMAGE (variant: $VARIANT)"
    docker_build
    section_end "$SECTION_DBUILD"
    test "${CI_JOB_STAGE:-}" = build || return 0
    test -n "${DOCKER_BUILD_IMAGE:-}" \
        || die "DOCKER_BUILD_IMAGE is not set - is the GitLab CI registry enabled?"
    BUILD_TAGS=("$COMMIT_SHA" "$COMMIT_REF")
    test "$CI_COMMIT_BRANCH" != "$CI_DEFAULT_BRANCH" || BUILD_TAGS+=("latest")
    for BUILD_TAG in "${BUILD_TAGS[@]}"; do
        test -n "${DOCKER_IMAGE_DEV:-}" && \
        docker_push_dev "$DOCKER_IMAGE_DEV" "$DOCKER_BUILD_IMAGE/dev:$(get_docker_tag "$BUILD_TAG")"
        docker_push_dev "$DOCKER_IMAGE"     "$DOCKER_BUILD_IMAGE:$(get_docker_tag "$BUILD_TAG")"
    done
}

test_python() {
    # assume that the job needs the image if the repo has one
    if [[ -f "$DOCKER_FILE" ]]; then
        pull_build_image || build_docker
    fi
    # track status in var in case we run both mypy & pytest
    STATUS=0
    # only run mypy if it's referenced in pyproject
    if grep "^mypy" pyproject.toml &>/dev/null; then
        mypy || STATUS=1
    fi
    pytest || STATUS=1
    # fail if any of the tests failed
    return "$STATUS"
}

publish_docker() {
    docker_login
    VARIANT="${DOCKER_VARIANT:-default}"
    SECTION_DPUB="publish_docker_$VARIANT"
    section_start "$SECTION_DPUB" "Publishing docker image (variant: $VARIANT)"
    BUILD_IMAGE="$DOCKER_BUILD_IMAGE:$(get_docker_tag "$COMMIT_SHA")"
    log "Using build image $BUILD_IMAGE"
    FLUX_TAG="$KEY_COMMIT.$CI_PIPELINE_ID"
    # tag and push docker images (on git tags, main branch, MRs)
    if [[ "$COMMIT_REF" = "$CI_COMMIT_TAG" ]]; then
        log "Publishing for CI_COMMIT_TAG=$CI_COMMIT_TAG"
        docker_push_prod "$CI_COMMIT_TAG"            # 1.2.3  (pinned)
        if grep -Eq "$RE_V_PATCH" <<<"$CI_COMMIT_TAG"; then
            log "Tagging minor semver version"       # 1.2    (floating)
            docker_push_prod "$(grep -Eo "^$RE_V_MINOR" <<<"$CI_COMMIT_TAG")"
        fi
        if grep -Eq "$RE_V_MINOR" <<<"$CI_COMMIT_TAG"; then
            log "Tagging major semver version"       # 1      (floating)
            docker_push_prod "$(grep -Eo "^$RE_V_MAJOR" <<<"$CI_COMMIT_TAG")"
        fi
    elif [[ "$COMMIT_REF" = "$CI_DEFAULT_BRANCH" ]]; then
        log "Publishing for CI_DEFAULT_BRANCH=$CI_DEFAULT_BRANCH"
        docker_push_prod "$COMMIT_SHA"  # d34db33f             (pinned)
        docker_push_prod "$KEY_COMMIT"  # main.d34db33f        (pinned)
        docker_push_prod "$FLUX_TAG"    # main.d34db33f.12345  (pinned)
        docker_push_prod "$COMMIT_REF"  # main                 (floating)
        docker_push_prod latest         # latest               (floating)
        echo "$DOCKER_IMAGE" | grep -Eq "^flywheel/" || return 0
        test -z "${DOCKER_VARIANT:-}" || return 0
        log "Updating $DOCKER_IMAGE dockerhub README"
        printf "\n## Repository\n\n[%s](%s)" "$CI_PROJECT_PATH" "$CI_PROJECT_URL" >>README.md
        if ! docker-pushrm "$DOCKER_IMAGE"; then
            warn "Cannot update image README - does the 'bot' team have 'Admin' perms?"
            echo "See https://hub.docker.com/repository/docker/$DOCKER_IMAGE/permissions"
        fi
    elif [[ "$COMMIT_REF" =~ ^(release|update)-.*$ ]]; then
        log "Skipping publish for COMMIT_REF=$COMMIT_REF"
    else
        log "Publishing for COMMIT_REF=$COMMIT_REF"
        docker_push_prod "$COMMIT_SHA"  # d34db33f               (pinned)
        docker_push_prod "$KEY_COMMIT"  # branch.d34db33f        (pinned)
        docker_push_prod "$FLUX_TAG"    # branch.d34db33f.12345  (pinned)
        docker_push_prod "$COMMIT_REF"  # branch                 (floating)
    fi
    section_end "$SECTION_DPUB"
}

publish_helm() {
    log "Publishing helm chart"
    set_version
    helm_init
    cd "$HELM_DIR" || die "Cannot cd into $HELM_DIR"
    helm dependency build 2>/dev/null || :
    helm repo add flywheel-local http://helm-chartmuseum:8080
    helm-cm-push -f . flywheel-local || die "Couldn't push to chartmuseum"
    test -n "${CI_COMMIT_TAG:-}" && CI_HELM_CHANNEL=tags || CI_HELM_CHANNEL="$CI_COMMIT_BRANCH"
    CI_HELM_REGISTRY_URL="https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/packages/helm/$CI_HELM_CHANNEL"
    # shellcheck disable=SC2015
    helm repo add gitlab --username=gitlab-ci-token --password="$CI_JOB_TOKEN" "$CI_HELM_REGISTRY_URL" && \
    helm-cm-push -f . gitlab || warn \
"Could not push chart to the CI registry on ref-based channel $CI_HELM_CHANNEL
To fix, enable the package registry for the project and use short branch names."
}

publish_npm() {
    die "Not implemented"
    log "Publishing npm package"
    set_version
    # TODO revisit public/private target configuration
    # npm version --no-git-tag-version "$CI_COMMIT_TAG"  # TODO non-tag
    # npm run build
    # if [[ "$CI_PROJECT_VISIBILITY" = "public" ]]; then
    #     log "Publishing public node package to npm"
    # else
    #     log "Publishing private node package to the GitLab package registry"
    # fi
}

generate_docs() {
    # temporary backwards-compat w/ generate_docs defined in old hooks
    # a) only do some steps in ci
    if "$CI"; then
        # update the version in tracked files
        set_version
        # assume that the job needs the image if the repo has one
        ! test -f "$DOCKER_FILE" || pull_build_image
    fi
    # b) port the command from the old pre-commit hook
    if grep -q generate_docs .pre-commit-config.yaml; then
        YQ_FILT='.repos[0].hooks[]|select(.id=="generate_docs")|.args[]'
        HOOK_CMD="$(yq -r "$YQ_FILT" .pre-commit-config.yaml)"
        GENERATE_DOCS_CMD="${GENERATE_DOCS_CMD:-$HOOK_CMD}"
    fi
    section_start generate_docs "Generating docs into public/"
    mapfile -t CMD < <(sed -E 's/ /\n/g' <<<"${GENERATE_DOCS_CMD:-docker_run mkdocs build}")
    log "Running ${C_CYAN}${CMD[*]}${C_RESET}"
    "${CMD[@]}" || die "Couldn't generate docs, command exited with $?"
    section_end generate_docs
}

publish_poetry() {
    log "Publishing python package with poetry"
    set_version
    poetry build --format wheel
    if [[ "$CI_PROJECT_VISIBILITY" = "public" ]]; then
        log "Publishing public python package to PyPI"
        poetry publish -u "$PYPI_USER" -p "$PYPI_PASS"
    else
        log "Publishing private python package to the GitLab package registry"
        PYPI_USER="gitlab-ci-token"
        PYPI_PASS="$CI_JOB_TOKEN"
        PYPI_REPO="$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/pypi"
        poetry config "repositories.$CI_PROJECT_NAME" "$PYPI_REPO"
        poetry publish -u "$PYPI_USER" -p "$PYPI_PASS" -r "$CI_PROJECT_NAME"
    fi
}

# create release branch and mr for a new project version (manual trigger)
release_mr() {
    log "Running release MR job with the following initial config:"
    log "> RELEASE=${RELEASE:-}"
    log "> CHERRY_PICK=${CHERRY_PICK:-}"
    log "> TARGET_BRANCH=${TARGET_BRANCH:-}"
    # validate the release version input
    RELEASE_REGEX="${GEAR_VERSION_STYLE:-$RE_VERSION}"
    grep -Eqx "$RELEASE_REGEX" <<<"$RELEASE" \
        || die "Invalid release '$RELEASE': doesn't match '$RELEASE_REGEX'"
    # auth as the git bot and fetch project history
    git_login
    git fetch --tags --unshallow &>/dev/null || :
    git checkout "$CI_DEFAULT_BRANCH"
    # detect the last x.y.z version on the x.y line (if any)
    RELEASE_MINOR=$(grep -Eo "^$RE_V_MINOR" <<<"$RELEASE")
    PREV_RELEASE=$(git tag --list "$RELEASE_MINOR*" \
        | grep -Ex "$RE_V_PATCH" | sort -rV | head -n1 || :)
    # when given only x.y, auto-detect the next .z
    if [[ "$RELEASE" = "$RELEASE_MINOR" ]]; then
        test -n "$PREV_RELEASE" \
            && RELEASE=$(inc_ver "$PREV_RELEASE") \
            || RELEASE=$RELEASE_MINOR.0
        log "Using auto-picked patch for $RELEASE_MINOR: '$RELEASE'"
    fi
    # when there's no prev release in the x.y, use the latest tag
    test -n "$PREV_RELEASE" || PREV_RELEASE=$(git tag --list \
        | grep -Ex "$RE_V_PATCH" | sort -rV | head -n1 || :)
    # ensure that the new release is greater than the previous one
    test -z "$PREV_RELEASE" || vergt "$RELEASE" "$PREV_RELEASE" \
        || die "Invalid release '$RELEASE': must be newer than '$PREV_RELEASE'"
    # auto-detect target branch (main for feature release, x.y for patches)
    if [[ -n "${TARGET_BRANCH:-}" ]]; then
        log "Using user-selected target branch '$TARGET_BRANCH'"
    else
        grep -Eq "\.0$" <<<"$RELEASE" \
            && TARGET_BRANCH=$CI_DEFAULT_BRANCH \
            || TARGET_BRANCH=$RELEASE_MINOR
        log "Using auto-picked target branch '$TARGET_BRANCH'"
    fi
    # checkout target branch (optionally auto-create from the last release)
    if git checkout "$TARGET_BRANCH" &>/dev/null; then
        log "Using existing target branch '$TARGET_BRANCH'"
    else
        log "Creating new target branch '$TARGET_BRANCH' (from '$PREV_RELEASE')"
        git checkout -b "$TARGET_BRANCH" "$PREV_RELEASE"
        git push || die "Could not push to $TARGET_BRANCH"
    fi
    # ensure that the x.y target branch contains the latest x.y.z tag
    LAST_TAG=$(git tag --merged=HEAD --sort=-v:refname | head -n1 || :)
    if [[ "$TARGET_BRANCH" = "$RELEASE_MINOR" && "$LAST_TAG" != "$PREV_RELEASE" ]]; then
        log "Attempting to merge rogue tag '$PREV_RELEASE' into '$RELEASE_MINOR'"
        if git merge "$PREV_RELEASE"; then
            git push
        else
            warn "Could not reconcile rogue tag '$PREV_RELEASE'"
            git merge --abort
        fi
    fi
    # cherry-pick commits/tickets listed in CHERRY_PICK from the main branch
    if [[ -n "${CHERRY_PICK:-}" ]]; then
        PATTERN=$(sed -E 's/[, ]+/|/g;s/^|//;s/|$//' <<<"$CHERRY_PICK")
        git_log() { git log --first-parent --format=%h --abbrev=8 "$@"; }
        git_show() { git show --no-patch --format='%H%n%B' "$1"; }
        for REV in $(git_log "$CI_DEFAULT_BRANCH" | head -n500 | tac); do
            REV_TEXT=$(mktemp)
            git_show "$REV" > "$REV_TEXT"
            if grep -Eq "$PATTERN" "$REV_TEXT"; then
                git cherry-pick -m1 "$REV"
            fi
        done
    fi
    # bump the version in files that track it
    set_version "$RELEASE"
    # if there is no pyproject.toml/package.json/etc then write VERSION
    # (this allows creating an MR with a non-empty diff)
    has_diff || echo "$RELEASE" >VERSION
    # open an MR for release-x.y.z
    git_mr "release-$RELEASE" "$TARGET_BRANCH" \
        title="Release $RELEASE" \
        description="$(get_release_desc "$PREV_RELEASE")" \
        labels="qa-ci::release"
}

# create project tag when release mr merges
release_tag() {
    git_login
    RELEASE="$(echo "$CI_COMMIT_MESSAGE" | sed -En "s|[^']*'release-([^']*).*|\1|p ")"
    if git fetch origin tag "$RELEASE" --no-tags &>/dev/null; then
        log "Tag $RELEASE already exists"
        git diff-index --quiet "$RELEASE" -- || die "The existing tag differs"
    else
        log "Creating tag $RELEASE"
        git tag -a "$RELEASE" -m "$RELEASE"
        git push origin "$RELEASE"
        # on initial RC's of umbrella components, auto-create branch for patches
        if [[ -n "$FW_COMPONENT" ]] && grep -Eq "$D+\.$D\.0" <<<"$RELEASE"; then
            MINOR_BRANCH="$(grep -Eo "$RE_V_MINOR" <<<"$RELEASE")"
            git checkout -B "$MINOR_BRANCH"
            git push origin "$MINOR_BRANCH" || :
        fi
    fi
}

# use autoupdate.sh on the current project
update_deps() {
    git_login
    MR_DESC="$(mktemp)"
    /qa-ci/scripts/autoupdate.sh >"$MR_DESC"
    git_mr update-deps "$CI_COMMIT_BRANCH" \
        title="Auto-update dependencies" \
        description="$(cat "$MR_DESC")" \
        labels="qa-ci::update"
}

# trigger downstream project pipelines like autoupdates
update_triggers() {
    for PROJECT in $UPDATE_PROJECTS; do
        gitlab_trigger "$PROJECT" main UPDATE=true || warn "Could not trigger $PROJECT"
    done
}

# create a gitlab project release for a tag
# for components, also bump the version in umbrella (on rc's)
# for gears, also bump the version in gear-exchange
update_release() {
    MR_ID="$(grep -Eo "$CI_PROJECT_PATH![0-9]+" <<<"$CI_COMMIT_MESSAGE" \
        | head -n1 | sed -E 's/^.*!//')"
    MR_DESC="$(mktemp)"
    gitlab_proj_api get "/merge_requests/$MR_ID" \
        | jq -r .description \
        | sed -E "s|\.\.\.[^?]+\?|...$CI_COMMIT_TAG?|" \
        >"$MR_DESC"
    log "Creating $CI_PROJECT_NAME GitLab release $CI_COMMIT_TAG"
    gitlab_proj_api post /releases --raw "$(jo \
        name="$CI_PROJECT_NAME $CI_COMMIT_TAG ($(date +%F))" \
        tag_name="$CI_COMMIT_TAG" \
        description="$(get_section PROJECT_RELEASE "$MR_DESC")" \
    )" | jq -r ._links.self
    if [[ -n "$FW_COMPONENT" ]]; then
        bump_umbrella_component_version
    elif [[ -f "$DOCKER_FILE" && -f manifest.json && "${SKIP_EXCHANGE,,}" != "true" ]]; then
        /qa-ci/scripts/gearexchange.sh
    fi
}

bump_umbrella_component_version() {
    mapfile -t JIRAS < <(grep -Eio "$RE_JIRA" "$MR_DESC"|sort -uV)
    mapfile -t FW_RELS < <(get_section FLYWHEEL_RELEASE "$MR_DESC" \
        | tr "[:upper:]" "[:lower:]" | sed -En 's/^- \[x\] (.*)/\1/p')
    test "${#FW_RELS[@]}" -gt 0 || { log "No umbrella releases to update."; return 0; }
    log "Bumping $CI_PROJECT_NAME chart version in umbrella ${FW_RELS[*]}"
    git_login
    git_clone "$FW_UMBRELLA" --no-single-branch
    RELEASECI_VAR="RELEASECI_$(upper "${FW_COMPONENT//-/}")_VERSION"
    for FW_REL in "${FW_RELS[@]}"; do
        log "Bumping $CI_PROJECT_NAME to $CI_COMMIT_TAG in umbrella $FW_REL"
        git checkout -f "$FW_REL"
        sed -Ei "s/($RELEASECI_VAR: ).*/\1$CI_COMMIT_TAG/I" .gitlab-ci.yml
        git add --update
        git commit -m "$(cat <<EOF
fix: bump $FW_COMPONENT to $CI_COMMIT_TAG

${JIRAS[*]}
EOF
)"
        git push origin "$FW_REL"
    done
    case "$FW_COMPONENT" in
        cli)       JIRA_COMPONENT="CLI (Legacy)";;
        connector) JIRA_COMPONENT="Xfer Connector";;
        *)         JIRA_COMPONENT=${FW_COMPONENT^};;
    esac
    test -n "${JIRA_COMPONENT_RELEASE_HOOK_URL:-}" || return 0
    test -n "${JIRA_COMPONENT_RELEASE_HOOK_TOKEN:-}" || return 0
    mapfile -t FW_VERS < <(printf "%s\n" "${FW_RELS[@]}" | sed -E "s/release-//")
    xh -IF "$JIRA_COMPONENT_RELEASE_HOOK_URL" \
        x-automation-webhook-token:"$JIRA_COMPONENT_RELEASE_HOOK_TOKEN" \
        issues:="$(jo -a "${JIRAS[@]}")" \
        data:="$(yq -oj . <<<"
            component: $JIRA_COMPONENT
            fixVersions: $(jo -a "${FW_VERS[@]}")
        ")" \
    || warn "Jira component release webhook failed"
}

# update dev deployments
update_deployment() {
    # track the umbrella minor version of an existing deployment
    if [[ -n "${TRACK:-}" ]]; then
        git_clone "$TRACK"
        UPDATE_VERSION_RE="$(
            yq -r .variables.FLYWHEEL_VERSION .gitlab-ci.yml | \
            grep -Eo '^[0-9]+\.[0-9]+'
        )"
        cd "$CI_PROJECT_DIR" || exit 1  # undo git_clone's auto-cd
    # trigger a fresh umbrella dev build when using FORCE=true
    elif [[ "${FORCE:-}" = true ]]; then
        mapfile -t VERSION_VARS < <(env | grep -E 'VERSION=' | sort)
        log "Triggering umbrella@dev with vars:$(printf "\n  %s" "${VERSION_VARS[@]}")"
        PIPELINE_URL="$(gitlab_trigger "$FW_UMBRELLA" dev "${VERSION_VARS[@]}")"
        PIPELINE_ID="${PIPELINE_URL//*\/}"
        UPDATE_VERSION_RE=$PIPELINE_ID
        log "Triggered $PIPELINE_URL"
        while true; do
            PIPELINE_STATUS="$(gitlab_api get "$FW_UMBRELLA" "/pipelines/$PIPELINE_ID" | jq -r .status)"
            log "umbrella build status: $PIPELINE_STATUS"
            case "$PIPELINE_STATUS" in
                success)                 break;;
                failed|canceled|skipped) die "Umbrella build failed";;
                *)                       sleep 60;;
            esac
        done
    fi
    for ATTEMPT in {1..5}; do
        log "Retrieving flywheel helm chart index..."
        # retrieve the fw chart index and extract the flywheel (umbrella) entries
        xh -IF https://helm.dev.flywheel.io/index.yaml | yq .entries.flywheel >/tmp/entries
        # get semver-sorted flywheel chart versions (newest 1st, semantic pre-releases)
        yq ".[].version" /tmp/entries | vsort >/tmp/versions
        # exclude pre-releases if $UPDATE_PRE_RELEASE != true (default: include)
        test "${UPDATE_PRE_RELEASE:-true}" = true || sed -Ei "/-/d" /tmp/versions
        # filter using an arbitrary regex if set (useful for picking dev builds)
        test -z "$UPDATE_VERSION_RE" || sed -Ei "/$UPDATE_VERSION_RE/!d" /tmp/versions
        # give some breathing room for chartmuseum
        if [[ -s /tmp/versions ]]; then
            break
        elif [[ "$ATTEMPT" -lt 5 ]]; then
            log "Waiting a minute to let chartmuseum index any new charts"
            sleep 60
        else
            echo "UPDATE_PRE_RELEASE=${UPDATE_PRE_RELEASE:-}"
            echo "UPDATE_VERSION_RE=${UPDATE_VERSION_RE:-}"
            die "Couldn't find umbrella chart matching the constraints"
        fi
    done
    # pick the latest available given the constraints
    FLYWHEEL_VERSION="$(head -n1 /tmp/versions)"
    # update the FLYWHEEL_VERSION gitlab-ci variable with the selected version
    yq -i ".variables.FLYWHEEL_VERSION=\"$FLYWHEEL_VERSION\"" .gitlab-ci.yml
    # track the flywheel, component, flux- and image-override versions in versions.yml
    test -f versions.yaml && VERSIONS=versions.yaml || VERSIONS=versions.yml
    echo "flywheel: $FLYWHEEL_VERSION" >"$VERSIONS"
    yq ".[]|select(.version==\"$FLYWHEEL_VERSION\")|.dependencies[]|{.name:.version}" /tmp/entries | sort >>"$VERSIONS"
    while IFS=$'\n' read -r OVERRIDE; do
        NAME="${OVERRIDE// = */}"
        yq -i ".\"$NAME\"=.\"$NAME\" + \" (flux: ${OVERRIDE//* = /})\"" "$VERSIONS"
    done < <(yq -Nop 'select(.kind=="HelmRelease")|.spec.chart.spec|{.chart:.version}' flux/*.yaml)
    while IFS=$'\n' read -r OVERRIDE; do
        NAME="${OVERRIDE// = */}"
        yq -i ".\"$NAME\"=.\"$NAME\" + \" (image: ${OVERRIDE//* = /})\"" "$VERSIONS"
    done < <(yq -op 'to_entries|.[]|select(.value.image.tag)|{.key:.value.image.tag}' values.yaml)
    # allow consecutive auto-updates and push any changes
    ALLOW_CONSECUTIVE_BOT_COMMIT=true
    git_push "$CI_COMMIT_REF_NAME" "Deployment auto-update"
}

# check a deployment w/ some http requests then running the qa-automation suite
check_deployment() {
    DOMAIN="$(yq .global.domain values.yaml)"
    log "Sending smoke-test HTTP requests to $DOMAIN"
    xh -IF "https://$DOMAIN"             >/tmp/xh || die "frontend down"
    xh -IF "https://$DOMAIN/api/version" >/tmp/xh || die "core-api down"
    VHELM="$(yq .flywheel versions.yml)"
    VCORE="$(yq .flywheel_release /tmp/xh)"
    test "$VCORE" = "$VHELM" || die "versions.yml|.flywheel=$VHELM != /api/version|.flywheel_release=$VCORE"
    # TODO ^ extend the above checks as much as possible
    export QA_ENVIRONMENT="$DOMAIN"
    export QA_BRANCH="${QA_BRANCH:-master}"
    export QA_CLI_VERSION="${QA_CLI_VERSION:-stable}"
    mapfile -t QA_VARS < <(env | grep -E '^QA_' | grep -Ev '^QA_CI' | sed -E 's/^QA_//' | sort)
    log "Triggering qa-automation w/ ${QA_VARS[*]}"
    PIPELINE_URL="$(gitlab_trigger flywheel-io/qa/qa-automation "$QA_BRANCH" "${QA_VARS[@]}")"
    PIPELINE_ID="${PIPELINE_URL//*\/}"
    log "Triggered $PIPELINE_URL"
}

# retrieve and save qa-automation html test results as gitlab-pages
publish_deployment_test_results(){
    rm -rf public; mkdir -p public
    qa_get(){ gitlab_api get flywheel-io/qa/qa-automation "$@"; }
    q="?ref=${QA_BRANCH:-master}&source=pipeline&scope=finished"
    j_filt='.[]|select(.name=="xfer" and .status|test("success|failed"))|.id'
    for pid in $(qa_get "/pipelines$q" | yq -r '.[].id'); do
        jid=$(qa_get "/pipelines/$pid/jobs" | yq -r "$j_filt" | head -n1)
        test -n "$jid" && break
    done
    test -n "$jid" || die "No recent qa-automation pipelines found w/ an xfer job"
    qa_get "/jobs/$jid" >job.json
    yq_job(){ yq -r "$@" job.json; }
    log "Downloading QA report for $(yq_job .status) job $(yq .id)"
    qa_get "/jobs/$jid/artifacts" >artifacts.zip
    unzip -qq artifacts.zip
    find report -name '*[0-9].html' -print0 | xargs -0I% cp -f % public
    cp -f public/report*.html public/index.html
    tree public
}


# HELPERS

# set up the repo for ci commits and pushes (configure user/email/origin)
git_login() {
    ! test -f "/tmp/git_login_$CI_JOB_ID" || return 0  # run only once per job
    log "Configuring git with flywheel-bot credentials"
    git config --global user.name "${CI_PUSH_USER_NAME:?CI variable required}"
    git config --global user.email "${CI_PUSH_USER_EMAIL:?CI variable required}"
    git config --global --add --bool push.autoSetupRemote true
    echo >/dev/null "${CI_PUSH_TOKEN:?CI variable required}"
    git remote set-url origin "$(git_url "$CI_PROJECT_URL")"
    touch "/tmp/git_login_$CI_JOB_ID"
}

# print any git diff in the pwd/repo (return 0 if there is no diff)
git_diff() { git --no-pager diff -U0 --word-diff=color --exit-code HEAD; }
has_diff() { ! git_diff &>/dev/null; }

# commit and push any outstanding diff on the current branch
# shellcheck disable=SC2120
git_push() {
    has_diff || { log "Nothing to commit"; return; }
    git_login
    BRANCH="${1:-$CI_COMMIT_BRANCH}"
    MESSAGE="${2:-Auto-fix}"
    git fetch --unshallow &>/dev/null || true
    git branch -D "$BRANCH" &>/dev/null || true
    git checkout "$BRANCH"
    # avoid bot commit loops unless explicitly allowed
    # eg. consecutive nightly auto-deploys are OK
    if [[ "${ALLOW_CONSECUTIVE_BOT_COMMIT:-}" != true ]]; then
        git log -1 --pretty="%an:%s" | grep -vq "flywheel-bot:$MESSAGE" \
            || die "Refusing to push consecutive flywheel-bot commit"
    fi
    section_start git_push "Pushing '$MESSAGE' to $BRANCH"
    git_diff || true  # print the diff for debugging in ci
    git add "--${GIT_ADD:-update}"
    git commit --message "$MESSAGE"
    git push origin "$BRANCH" "${@:3}"
    section_end git_push
}

# create a new branch an open an mr from any outstanding diff
git_mr() {
    ! git_diff || { log "Nothing to commit"; return 0; }
    SOURCE_BRANCH="$1"
    TARGET_BRANCH="$2"
    log "Pushing changes to $SOURCE_BRANCH"
    git checkout -B "$SOURCE_BRANCH"
    git add "--${GIT_ADD:-update}"
    git commit --message "$SOURCE_BRANCH"
    git push --force origin "$SOURCE_BRANCH" \
        || die "Couldn't push branch $SOURCE_BRANCH"
    log "Creating $SOURCE_BRANCH MR targeting $TARGET_BRANCH"
    OUTERR="$(mktemp)"
    gitlab_proj_api post /merge_requests --raw "$(jo -- \
        -s "source_branch=$SOURCE_BRANCH" \
        -s "target_branch=$TARGET_BRANCH" \
        "remove_source_branch=true" \
        "${@:3}" \
    )" &>"$OUTERR" && STATUS=0 || STATUS="$?"

    if [[ "$STATUS" = 0 ]]; then
        log "Created new $SOURCE_BRANCH MR"
    elif grep -Eq "error: 409|Another open merge request already exists for this source branch" "$OUTERR"; then
        MR_ID=$(gitlab_proj_api get /merge_requests state==opened source_branch=="$SOURCE_BRANCH" \
            | jq -r '.[]|.iid' | head -n1)
        gitlab_proj_api put /merge_requests/"$MR_ID" "${@:3}"
        log "Updated existing $SOURCE_BRANCH MR"
    else
        err "Could not create $SOURCE_BRANCH MR"
        cat "$OUTERR" >&2
        return "$STATUS"
    fi
}

# jira api client
# https://developer.atlassian.com/cloud/jira/platform/rest/v3/intro/#about
jira_api() {
    xh -IF "$1" "https://flywheelio.atlassian.net/rest/api/3$2" \
        "authorization:Basic $JIRA_CI_TOKEN" \
        "content-type:application/json" \
        "${@:3}"
}

# gitlab api client - https://docs.gitlab.com/ee/api/index.html#rest-api
# $1=http_method / $2=project / $3=api_endpoint / $4+=xh_args_eg_payload
# TODO switch to glab
# - do not enforce explicit http method to minimize posargs
# - support quoting project in path (given as a single arg)
# - default to current project if not in the path prefix
# - do not default to ctype=json for repo archives / job artifacts
# - do not default to bot token auth header on pipeline triggers
# - support defaulting the api baseurl for local testing
gitlab_api() {
    xh -IF "$1" "$CI_API_V4_URL/projects/$(urlquote "$2")$3" \
        "Private-Token:$GITLAB_CI_BOT_TOKEN" \
        "Content-Type:application/json" \
        "${@:4}"
}

# gitlab api client bound to the current project
# $1=http_method / $2=api_endpoint / $3+=xh_args_eg_payload
gitlab_proj_api() { gitlab_api "$1" "$CI_PROJECT_ID" "${@:2}"; }

# gitlab pipeline trigger helper
# https://docs.gitlab.com/ee/api/pipeline_triggers.html#trigger-a-pipeline-with-a-token
# $1=project / $2=project_ref / $3+=variables
gitlab_trigger() {
    test "$#" -ge 2 || die "Project and revision required"
    echo "$*" | grep -q DEBUG= || set -- "$@" DEBUG="${DEBUG:-}"
    TRIGGER_VARS=()
    for KV in "${@:3}"; do TRIGGER_VARS+=("variables[${KV%%=*}]=${KV#*=}"); done
    xh -IF post "$CI_API_V4_URL/projects/$(urlquote "$1")/trigger/pipeline" \
        "Content-Type:application/json" \
        ref="$2" token="$CI_JOB_TOKEN" "${TRIGGER_VARS[@]}" \
        | jq -r .web_url
}

# gitlab job artifact getter
# $1=project / $2=job-id / $3+=unzip args
gitlab_artifact() {
    test "$#" -ge 2 || die "Project and job id required"
    FILE="gitlab-job-$2-artifacts.zip"
    xh -IFo"$FILE" "$CI_API_V4_URL/projects/$(urlquote "$1")/jobs/$2/artifacts" \
        "Private-Token:$GITLAB_CI_BOT_TOKEN"
    unzip -oqq "$FILE" "${@:3}"
    rm "$FILE"
}

# login to the available registries
docker_login() {
    ! test -f "/tmp/docker_login_$CI_JOB_ID" || return 0  # run only once per job
    section_start docker_login "Logging in to available docker registries"
    DOCKER_CONFIG="$HOME/.docker/config.json"
    mkdir -p "$(dirname "$DOCKER_CONFIG")"
    test -s "$DOCKER_CONFIG" || echo '{"auths":{}}' >"$DOCKER_CONFIG"
    for REGISTRY in GITLAB DOCKER HARBOR GOOGLE; do
        VNAME="${REGISTRY}_REGISTRY"
        VUSER="${REGISTRY}_REGISTRY_USER"; test "$REGISTRY" = GOOGLE || VUSER="${!VUSER}"
        VPASS="${REGISTRY}_REGISTRY_PASS"; VPASS="${!VPASS}"
        if [[ -n "${!VNAME:-}" && -n "${!VUSER:-}" && -n "${!VPASS:-}" ]]; then
            log "Setting up ${REGISTRY,,} registry creds"
            NAME="$(sed -E 's|docker.io|https://index.docker.io/v1/|' <<<"${!VNAME}")"
            CONFIG_KEY=".auths.\"$NAME\".auth"
            (   set +x  # set +x to avoid leaking creds in base64 when debugging
                BASIC_AUTH="$(echo -n "${!VUSER}:${!VPASS}" | base64 -w0)"
                jqi "$DOCKER_CONFIG" "$CONFIG_KEY = \"$BASIC_AUTH\""
            )
        else
            test "$VUSER" = _json_key_base64 && VARS="$VPASS" || VARS="$VUSER and $VPASS"
            log "Skipping ${REGISTRY,,} registry login: missing var $VARS"
        fi
    done
    touch "/tmp/docker_login_$CI_JOB_ID"
    section_end docker_login
}

# podman/docker push to the dev registry (ie. gitlab)
docker_push_dev(){
    SRC=$1; DST=$2
    SECTION_DPUSHDEV="docker_push_dev_${RANDOM}"
    section_start "$SECTION_DPUSHDEV" "Pushing image as $DST"
    if [[ "${DOCKER_BIN:-}" = podman ]]; then
        podman push "$SRC" "docker://$DST" || die "Could not push $DST"
    elif [[ "${DOCKER_BIN:-}" = docker ]]; then
        docker tag "$SRC" "$DST"
        docker push "$DST" || die "Could not push $DST"
    fi
    section_end "$SECTION_DPUSHDEV"
}

# skopeo copy from the dev to the prod registry (using $1 for the :tag)
docker_push_prod() {
    SRC="${DOCKER_BUILD_IMAGE}:$(get_docker_tag "$COMMIT_SHA")"
    VARIANTS="${DOCKER_VARIANT:-default} ${DOCKER_VARIANT_ALIAS:-}"
    mapfile -t VARIANTS < <(sed -E 's/ +/ /g;s/^ | $//;s/ /\n/g' <<<"$VARIANTS")
    for VARIANT in "${VARIANTS[@]}"; do
        TAG="$(DOCKER_VARIANT=${VARIANT/default} get_docker_tag "${1:-latest}")"
        DST="${DOCKER_IMAGE/#flywheel/docker.io\/flywheel}:$TAG"
        SECTION_DPUSHPROD="docker_push_prod_$RANDOM"
        section_start "$SECTION_DPUSHPROD" "Pushing image as $DST"
        skopeo copy --multi-arch=all "docker://$SRC" "docker://$DST" \
            || die "Could not push $DST"
        section_end "$SECTION_DPUSHPROD"
    done
}

# set the version in tracked files that reference it
# useful in publish jobs and before tagging a repository
set_version() {
    section_start set_version "Updating the project version in tracked files"
    TARGET_VER="${1:-${CI_COMMIT_TAG:-$("$CI" || git describe --exact-match --tags HEAD 2>/dev/null || :)}}"
    if [[ -z "$TARGET_VER" ]]; then
        NEXT_VER="$(git ls-remote -qt | sed -En "s|refs/tags/($RE_V_PATCH)$|\1|p" | cut -f2 | sort -rV | head -n1 || :)"
        NEXT_VER="$(inc_ver "${NEXT_VER:-0.1.0}")"
        PRE_RELEASE="${CI_PIPELINE_ID:-$(date -u +%s)}+$KEY_COMMIT"
        PRE_RELEASE_PREFIX="$(test "$CI_COMMIT_BRANCH" = "$CI_DEFAULT_BRANCH" && echo beta || echo alpha)"
    fi
    # VERSION file at the root of the repo
    if [[ -f VERSION ]]; then
        VERSION="${TARGET_VER:-$NEXT_VER-$PRE_RELEASE_PREFIX.$PRE_RELEASE}"
        log "Updating VERSION file to $VERSION"
        echo "$VERSION" >VERSION
    fi
    # nodejs package.json version bump
    # tags | 1.2.3
    # main | 1.2.4-beta.56789+main.d34db33f
    # feat | 1.2.4-alpha.56789+feat.d34db33f
    # NOTE only update when RELEASE is set to avoid build layer cache breaks
    if [[ -f package.json && -n "${RELEASE:-}" ]]; then
        VERSION="${TARGET_VER:-$NEXT_VER-$PRE_RELEASE_PREFIX.$PRE_RELEASE}"
        log "Updating package[-lock].json:version to $VERSION"
        jqi package.json ".version = \"$VERSION\""
        npm install --package-lock-only
    fi
    # python pyproject.toml version bump
    # tags | 1.2.3
    # main | 1.2.4b56789+main.d34db33f
    # feat | 1.2.4a56789+feat.d34db33f
    if [[ -f pyproject.toml ]]; then
        VERSION="${TARGET_VER:-$NEXT_VER${PRE_RELEASE_PREFIX:0:1}$PRE_RELEASE}"
        VERSION="${VERSION%%_*}"  # strip double gear versioning
        VERSION="${VERSION//--/-}"  # collapse double dashes to a single one
        VERSION="${VERSION//-rc/rc}"  # make rc's python-compatible
        log "Updating pyproject.toml:version to $VERSION"
        sed -Ei "s|^(version =) .*|\1 \"$VERSION\"|" pyproject.toml
    fi
    # gear manifest.json version bump
    # tags   | 1.2.3
    # branch | branch.d34db33f
    if [[ -f manifest.json ]]; then
        VERSION="${TARGET_VER:-$KEY_COMMIT}"
        IMAGE="$(jq -r '.custom."gear-builder".image // ""' manifest.json)"
        IMAGE="${IMAGE:-flywheel/$PYPROJ_NAME:$VERSION}"
        log "Updating manifest.json:version to $VERSION"
        log "Updating manifest.json:custom.gear-builder.image to ${IMAGE/%:*/:$VERSION}"
        jqi manifest.json -r "
            .version = \"$VERSION\" |
            .custom.\"gear-builder\".image = \"${IMAGE/%:*/:$VERSION}\"
        "
    fi
    # helm Chart.yaml / values.yaml / README.md version bumps
    # tags | 1.2.3                           | image.tag: 1.2.3
    # main | 1.2.4-beta.56789+main.d34db33f  | image.tag: main.d34db33f
    # feat | 1.2.4-alpha.56789+feat.d34db33f | image.tag: feat.d34db33f
    if [[ -f "$HELM_DIR/Chart.yaml" ]]; then
        VERSION="${TARGET_VER:-$NEXT_VER-$PRE_RELEASE_PREFIX.${PRE_RELEASE//_/-}}"  # under2dash
        IMAGE="${TARGET_VER:-$KEY_COMMIT}"  # corresponding image version
        log "Updating $HELM_DIR/Chart.yaml:version to $VERSION"
        yq -i ".version = \"$VERSION\"" "$HELM_DIR/Chart.yaml"
        log "Updating $HELM_DIR/values.yaml:image.tag to $IMAGE"
        yq -i ".image.tag = \"$IMAGE\"" "$HELM_DIR/values.yaml"
        # use a sneaky sed shortcut instead of invoking helm-docs
        log "Updating $HELM_DIR/README.md"
        sed_readme() { sed -Ei "s|$1|$2|" "$HELM_DIR/README.md"; }
        sed_readme "\[Version: .*\]"              "[Version: $VERSION]"
        sed_readme "(Version-).*(-informational)" "\1${VERSION/-/--}\2"
        sed_readme " image.tag (.*\`\").*(\"\`.*)"  " image.tag \1$IMAGE\2"
    fi
    section_end set_version
}

# print release MR description
get_release_desc() {
REV1=${1:-$(git rev-list --max-parents=0 HEAD | head -c8)}  # default: initial commit
REV2=${2:-$(git show --no-patch --format=%H HEAD | head -c8)}  # default: HEAD
CHANGELOG="$(mktemp)"
git_changelog "$REV1" "$REV2" >"$CHANGELOG"
mapfile -t ISSUES < <(grep -Eio "$RE_JIRA" "$CHANGELOG" | sort -uV)
ISSUES_STR=${ISSUES[*]}
ISSUES_JQL="key%20in%20%28${ISSUES_STR// /%2C}%29%20order%20by%20priority%2Ckey"
ISSUES_URL="https://flywheelio.atlassian.net/issues/?jql=$ISSUES_JQL"
CHANGES_URL="$CI_PROJECT_URL/-/compare/$REV1...$REV2?from_project_id=$CI_PROJECT_ID&straight=false"

# mark component issues already released in previous back-ports
if [[ -n "$FW_COMPONENT" ]]; then
    DONE_JQL="key in (${ISSUES_STR// /,}) and statuscategory=done"
    DONE_JSON=$(mktemp)
    jira_api get /search/jql jql=="$DONE_JQL" fields==status,fixVersions | jq .issues >"$DONE_JSON"
    for KEY in $(jq -r '.[]|.key' "$DONE_JSON"); do
        sed -Ei "s/$KEY/~$KEY~/g" "$CHANGELOG"
        for FIXVER in $(jq -r ".[]|select(.key==\"$KEY\")|.fields.fixVersions[].name" "$DONE_JSON" | grep -E "$RE_V_PATCH" | sort -rV); do
            verlt "${FIXVER%.*}" "${RELEASE%.*}" || continue
            sed -Ei "s/(.*$KEY.*)/\1 *(also in ${FIXVER%.*})*/" "$CHANGELOG"
            break
        done
    done
fi

cat <<EOF
<!-- PROJECT_RELEASE_BEGIN -->
#### Changelog ([GitLab]($CHANGES_URL)|[Jira]($ISSUES_URL))

$(
test -s "$CHANGELOG" \
&& cat "$CHANGELOG" \
|| echo "No changes."
)
<!-- PROJECT_RELEASE_END -->
EOF

# stop here unless it's a component
test -n "$FW_COMPONENT" || return 0

# designate and prepare the umbrella release mr for bumping
MINOR=${RELEASE%.*}
release_branches() { gitlab_api get "$FW_UMBRELLA" /merge_requests state==opened \
    | jq -r '.[].source_branch' | grep -Ex "release-$RE_V_PATCH" | sort -rV; }
# add a guard for umbrella auto-init - only for components known to follow minor ver
auto_release() { grep -Eqw "tools|snapshot|frontend" <<<"$CI_PROJECT_PATH"; }
if auto_release && ! release_branches | grep -Eq "release-$MINOR"; then
    PIPELINE_URL="$(gitlab_trigger "$FW_UMBRELLA" master RELEASE="$MINOR")"
    log "started umbrella release init pipeline for $MINOR:\n  $PIPELINE_URL"
    PIPELINE_ID="${PIPELINE_URL//*\/}"
    while true; do
        PIPELINE_STATUS="$(gitlab_api get "$FW_UMBRELLA" "/pipelines/$PIPELINE_ID" | jq -r .status)"
        log "umbrella release init pipeline status: $PIPELINE_STATUS"
        case "$PIPELINE_STATUS" in
            success|failed|canceled|skipped) break;;
            *)                               sleep 30;;
        esac
    done
fi
mapfile -t FW_RELS < <(release_branches)
cat <<EOF
<!-- FLYWHEEL_RELEASE_BEGIN -->
#### Flywheel release

Check all umbrella release MRs to include $CI_PROJECT_NAME $RELEASE in:

$(
test "${#FW_RELS[@]}" -gt 0 \
&& {
    printf -- "- [ ] %s\n" "${FW_RELS[@]}" | \
    sed -E "s/\[ \] (release-$MINOR\..*)/[x] \1/g"
} \
|| echo "No releases."
)
<!-- FLYWHEEL_RELEASE_END -->
EOF
}

# create gitlab-ci artifact placeholders if needed to avoid misleading errors
# TODO make the artifact part of the test job template more flexible instead
ensure_artifact() {
    for FILE in "$@"; do
        test "${FILE:0-1}" != / || FILE+=.keep
        mkdir -p "$(dirname "$FILE")"
        test -s "$FILE" || echo >>"$FILE"
    done
}

# ensure that $DOCKER_IMAGE is available
pull_build_image() {
    # skip on custom sse gear build+test job
    test "${CI_JOB_NAME:-}" = test:gear && return 1
    docker_login
    # NOTE in podman mode docker cli uses the compat socket
    image_exists(){ test -n "$(docker image ls -q "$1")"; }
    image_pull(){
        NAME=$1; PULL=$2
        ! image_exists "$NAME" || return 0
        if ! image_exists "$PULL"; then
            SECTION=pull_build_image_$RANDOM
            section_start "$SECTION" "Pulling build image $PULL"
            docker pull "$PULL" && STATUS=0 || STATUS=1
            section_end "$SECTION"
            if [[ "$STATUS" != 0 ]]; then
                warn "Could not pull $PULL"
                return "$STATUS"
            fi
        fi
        log "Tagging $PULL as $NAME for the job"
        docker tag "$PULL" "$NAME"
    }
    test -z "${DOCKER_IMAGE_DEV:-}" || \
    image_pull "$DOCKER_IMAGE_DEV" "$DOCKER_BUILD_IMAGE/dev:$COMMIT_SHA"
    image_pull "$DOCKER_IMAGE" "$DOCKER_BUILD_IMAGE:$(get_docker_tag "$COMMIT_SHA")"
}

# install the google cloud sdk to .cache (if not already installed)
gcloud_install() {
    INSTALL_DIR="$CI_PROJECT_DIR/.cache/google-cloud-sdk"
    export PATH="$INSTALL_DIR/bin:$PATH"
    ! which gcloud &>/dev/null || return 0
    section_start gcloud_install "Installing gcloud"
    INSTALL_DOCS=https://cloud.google.com/sdk/docs/install
    DOWNLOAD_RE='https://[^"]+linux-x86_64.tar.gz'
    DOWNLOAD_URL="$(xh -IF "$INSTALL_DOCS" | grep -Eo "$DOWNLOAD_RE" | head -n1)"
    xh -IF "$DOWNLOAD_URL" | tar xzC "${INSTALL_DIR%/*}"
    "$INSTALL_DIR/install.sh" \
        --path-update false \
        --bash-completion false \
        --usage-reporting false \
        --quiet >/dev/null
    rm -rf "$INSTALL_DIR/.install/.backup"
    section_end gcloud_install
}

# initialize the qa-ci job cache
# download data and register callback to upload it back at the end
qa_ci_cache_setup() {
    ! test -f "/tmp/qa_ci_cache_setup_$CI_JOB_ID" || return 0
    gcloud_install
    GS_KEY="${CI_COMMIT_BRANCH}/${DOCKER_VARIANT:-default}"
    export GS_TGZ="gs://qa-ci-cache/$CI_PROJECT_PATH/$GS_KEY.tgz"
    export FS_DIR="$CI_PROJECT_DIR/.qa-ci-cache"
    export QA_CI_CACHE_DIR="$FS_DIR"
    mkdir -p "$FS_DIR"
    echo "*" >"$FS_DIR/.gitignore"
    # wipe the cache on startup if clear is set
    if [[ "${CACHE_CLEAR:-}" = true ]]; then
        qa_ci_cache rm -f "$GS_TGZ"
    # try the branch-specific cache 1st
    elif qa_ci_cache cp "$GS_TGZ" - | mbuffer | tar xzC "$FS_DIR"; then
        true
    # fall back to the main branch from new feat branches
    elif [[ "$CI_DEFAULT_BRANCH" != "$CI_COMMIT_BRANCH" ]]; then
        GS_KEY2="${CI_DEFAULT_BRANCH}/${DOCKER_VARIANT:-default}"
        GS_TGZ2="gs://qa-ci-cache/$CI_PROJECT_PATH/$GS_KEY2.tgz"
        qa_ci_cache cp "$GS_TGZ2" - | mbuffer | tar xzC "$FS_DIR" || :
    fi
    # upload the cache (even on failure)
    add_exit_handler "tar czC '$FS_DIR' | mbuffer | qa_ci_cache cp - '$GS_TGZ'"
    # ensure that large files are uploaded in parallel
    cat >~/.boto <<EOF
[GSUtil]
parallel_composite_upload_threshold = 100M
EOF
    touch "/tmp/qa_ci_cache_setup_$CI_JOB_ID"
}

# qa-ci-cache helper - activating svc account / running gsutil cmd
qa_ci_cache() {
    gcloud config list account | grep -q qa-ci-cache || \
    gcloud auth activate-service-account --key-file "$QA_CI_CACHE_KEY"
    GSUTIL_CMD=(gsutil -m "$@")
    section_start qa_ci_cache "${GSUTIL_CMD[*]}"
    "${GSUTIL_CMD[@]}" && STATUS=0 || STATUS=1
    section_end qa_ci_cache
    return "$STATUS"
}
