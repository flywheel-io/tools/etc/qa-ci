#!/usr/bin/env bash
# hooks.sh - (sourceable) pre-commit hook functions


# wrapper for printing the logs in a section
hook() {(
    test "${DEBUG:-0}" -lt 3 || set -x  # DEBUG=3
    test "$#" -gt 0 || die "Command required"
    for CMD in "$@"; do echo "$CMD" | grep -Eq '^docker_run$' || break; done
    section_start "$CMD" "$CMD $CONTEXT"
    log "Running $*"
    if grep -Eqw "docker_build|generate_docs|mypy|pytest" <<<"$1"; then
        warn "Running $1 as a pre-commit hook is deprecated"
    fi
    "$@" && STATUS=0 || STATUS=$?
    section_end "$CMD"
    exit "$STATUS"
)}

# TODO add hook to verify that the qa-ci refs in pre-commit/gitlab are in sync

# aliases to qa-ci scripts
autoupdate() { /qa-ci/scripts/autoupdate.sh "$@"; }
eolfix() { /qa-ci/scripts/eolfix.py "$@"; }
gearcheck() { /qa-ci/scripts/gearcheck.sh "$@"; }
helmcheck() {
    export K8S_VERSION
    export HELM_DIR
    /qa-ci/scripts/helmcheck.sh "$@"
}
linkcheck() { /qa-ci/scripts/linkcheck.py "$@"; }
poetry_export() { /qa-ci/scripts/poetry_export.sh "$@"; }

# aliases to static linters with auto-injected config files
# NOTE using bash builtin 'command' to access the original cmd
markdownlint() {
    ls .markdownlint* &>/dev/null || set -- --config /qa-ci/.markdownlint-cli2.yaml "$@"
    command markdownlint-cli2 "$@"
}
yamllint() {
    test -s .yamllint.yml || set -- -c /qa-ci/.yamllint.yml "$@"
    command yamllint "$@"
}

# aliases to tools that are containerized via docker_run (deprecated)
mypy() {
    mkdir -p .cache/mypy  # mypy fails to auto-install types unless dir exists
    MYPY_ARGS=(
        --cache-dir=.cache/mypy
        --ignore-missing-imports
        --implicit-optional
        --install-types
        --non-interactive
        --color-output
        --pretty
        --show-column-numbers
        --show-error-context
        --show-error-codes
    )
    MYPY_FORCE_COLOR=1 docker_run mypy "${MYPY_ARGS[@]}" "$@"
}

pytest() {
    # backwards-compat: local docker build before/within pytest hook
    if [[ "${CI:-}" != true && -f Dockerfile ]]; then
        BUILD_TAG=${DOCKER_IMAGE_DEV:-$DOCKER_IMAGE}
        BUILD_TARGET=$(test -n "${DOCKER_IMAGE_DEV:-}" && echo dev)
        docker build . -t"$BUILD_TAG" --target="$BUILD_TARGET"
        # add additional local tags for SSE convenience
        if [[ -f manifest.json ]]; then
            GEAR_TAG="$(jq -r '.custom["gear-builder"].image' manifest.json)"
            GEAR_IMAGE="$(sed -E "s|:.*||" <<<"$GEAR_TAG")"
            docker tag "$BUILD_TAG" "$GEAR_TAG"
            docker tag "$BUILD_TAG" "$GEAR_IMAGE"
        fi
    fi
    PYTEST_ARGS=(
        "${PYTEST_DIR:-tests}"
        --color=yes
        --durations=5
        --cov="${PYPROJ_NAME//-/_}"
        --cov-fail-under="${PYTEST_COV_FAIL_UNDER:-$(
            grep -Eo "cov-fail-under=[0-9]+" pyproject.toml | grep -Eo "[0-9]+" \
            || echo 100
        )}"
        --no-cov-on-fail
        --cov-report=term-missing
        --cov-report=xml:coverage.xml
        --junitxml=junit.xml
        -o=junit_family=xunit2
    )
    test -z "${DEBUG:-}" || PYTEST_ARGS+=(-vvv)
    docker_run pytest "${PYTEST_ARGS[@]}" "$@"
}
