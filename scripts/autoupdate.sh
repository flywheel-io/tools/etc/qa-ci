#!/usr/bin/env bash
# autoupdate.sh - (executable) auto-update common project artifacts
set -Eeuo pipefail
USAGE="Usage: $0

Update common project artifacts. Usage via pre-commit try-repo:
> pre-commit try-repo https://gitlab.com/flywheel-io/tools/etc/qa-ci autoupdate

Dockerfile              Update the base image (eg.: FROM python:3.9.1 -> 3.9.x).
pyproject.toml          Update the build-system and loosen ^0.x dependency pins.
poetry.lock             Update the lock file based on the pyproject.toml.
requirements[-dev].txt  Update requirements files (from poetry.lock or itself).
.pre-commit-config.yaml Update pre-commit repo references (eg.: qa-ci rev).
.gitlab-ci.yml          Update gitlab-ci include references (eg.: qa-ci rev).

To skip updating one or more files listed above, add a var in .gitlab-ci.yml, eg.:
variables:
  UPDATE_SKIP: Dockerfile pyproject.toml
"
DIR="$(cd "${0%/*}/.." && pwd)"
. "$DIR/scripts/utils.sh"
test -z "${DEBUG:-}" || set -x


main() {
    echo "$*" | grep -Eqvw "help|--help|-h" || { echo "$USAGE"; exit; }

    # shellcheck disable=SC2076
    update_skip() { [[ ! -f "$1" || "${UPDATE_SKIP:-}" =~ "$1" ]]; }
    update_stat() { git diff -s --exit-code "$1" || update_log "- Updated \`$1\`"; }
    update_log()  { echo -e "$1"; }

    update_package_lock
    update_python
    update_pre_commit
    update_gitlab_ci
}


############################
# UPDATE PACKAGE-LOCK.JSON #
############################

update_package_lock() {
    if ! update_skip package-lock.json; then
        log "Updating package-lock.json"
        npm update
        update_stat package-lock.json
    fi
}


##################################################
# UPDATE PYPROJECT, POETRY LOCK AND REQUIREMENTS #
##################################################

update_python() {
    if ! update_skip pyproject.toml; then
        log "Updating pyproject.toml"
        sed -Ei 's/= "\^0\.[^"]*"/= "\^0"/' pyproject.toml
        sed -Ei 's/"poetry(-core)?>=.*"/"poetry-core>=1.0.8"/' pyproject.toml
        sed -Ei 's/poetry\.masonry\.api/poetry\.core\.masonry\.api/' pyproject.toml
        update_stat pyproject.toml
    fi
    if ! update_skip poetry.lock; then
        log "Updating poetry.lock"
        poetry update --lock >&2
        update_stat poetry.lock
        OUTDATED="$(poetry show --outdated | grep -v "Skipping virtualenv" || true)"
        if [[ -n "$OUTDATED" ]]; then
            update_log "\nSome poetry dependencies could not be updated:\n"
            update_log '```'
            update_log "$OUTDATED"
            update_log '```'
        fi
    fi
    if ! update_skip requirements.txt; then
        log "Updating requirements.txt"
        if [[ -f pyproject.toml ]]; then
            /qa-ci/scripts/poetry_export.sh >&2
        else
            PIP_COMPILE_CMD=(pip-compile --no-header --no-annotate --upgrade --resolver=backtracking)
            update_req() { sed -Ei "s/==.*//" "$1"; "${PIP_COMPILE_CMD[@]}" "$1" -o - | sponge "$1"; }
            update_req requirements.txt
            ! test -s requirements-dev.txt || update_req requirements-dev.txt
        fi
        update_stat requirements.txt
    fi
}


#####################
# UPDATE PRE-COMMIT #
#####################

update_pre_commit() {
    ! update_skip .pre-commit-config.yaml || return 0
    log "Updating .pre-commit-config.yaml"
    pre-commit autoupdate >&2
    update_stat .pre-commit-config.yaml
}


####################
# UPDATE GITLAB-CI #
####################

update_gitlab_ci() {
    for CI_YML in ${UPDATE_CI_YML:-.gitlab-ci.yml}; do
        ! update_skip "$CI_YML" || continue
        grep -q include: "$CI_YML" || continue
        log "Updating includes in $CI_YML"
        FILTER='.include[] | select(has("project") and has("ref")) | .project'
        for PROJECT in $(yq "$FILTER" "$CI_YML" | sort -u); do
            grep -q qa-ci <<<"$PROJECT" && REV="${UPDATE_QA_CI_REV:-HEAD}" || REV=HEAD
            SHA="$(
                git_clone "$PROJECT"
                if [[ "$REV" != "HEAD" ]]; then
                    git fetch origin "$REV":"$REV"
                    git checkout "$REV"
                fi
                git rev-parse "$REV"
            )"
            grep -En "project: ['\"]?$PROJECT" "$CI_YML" | while read -r LINE; do
                LINE_NO="$(echo "$LINE" | sed -E "s|^([0-9]+).*|\1|")"
                LINES="$((LINE_NO+1)),$((LINE_NO+2))"
                sed -Ei "${LINES}s|(ref: )[^[:space:]]+(.*)|\1$SHA\2|" "$CI_YML"
            done
            if [[ "$REV" != "HEAD" ]] && ! update_skip .pre-commit-config.yaml; then
                yq -i "(.repos[]|select(.repo|contains(\"qa-ci\")).rev) = \"$SHA\"" .pre-commit-config.yaml
            fi
        done
        update_stat "$CI_YML"
    done
}


main "$@"
