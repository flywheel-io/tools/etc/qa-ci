# supported qa-ci hooks

- id: autoupdate
  name: autoupdate
  description: |
    Update common project artifacts. Usage:
    > pre-commit try-repo https://gitlab.com/flywheel-io/tools/etc/qa-ci autoupdate

    Dockerfile              Update the base image (eg.: FROM python:3.9.1 -> 3.9.x).
    pyproject.toml          Update the build-system and loosen ^0.x dependency pins.
    poetry.lock             Update the lock file based on the pyproject.toml.
    requirements[-dev].txt  Update requirements files (from poetry.lock or itself).
    .pre-commit-config.yaml Update pre-commit repo references (eg.: qa-ci rev).
    .gitlab-ci.yml          Update gitlab-ci include references (eg.: qa-ci rev).

    To skip updating one or more files listed above, add a var in .gitlab-ci.yml, eg.:
    variables:
      UPDATE_SKIP: Dockerfile pyproject.toml
  language: script
  require_serial: true
  pass_filenames: false
  entry: scripts/run.sh hook autoupdate

- id: eolfix
  name: eolfix
  description: |
    Fix line endings in text files.

    - Replace CRLF and CR to LF
    - Trim whitespace before LF
    - Enforce one LF at the end
  types: [text]
  language: script
  require_serial: true
  entry: scripts/run.sh hook eolfix


- id: hadolint
  name: hadolint
  description: |
    Lint the project's Dockerfile and the shell commands within 'RUN' steps.
  types: [dockerfile]
  language: script
  require_serial: true
  entry: scripts/run.sh hook hadolint
  args: [
    --ignore, DL3005,  # apt upgrade
    --ignore, DL3008,  # apt install pin
    --ignore, DL3013,  # pip install pin
    --ignore, DL3016,  # npm install pin
    --ignore, DL3018,  # apk add pin
    --ignore, DL3059,  # consecutive RUN
  ]

- id: gearcheck
  name: gearcheck
  description: |
    Lint Flywheel gears, checking for expected files/versions and common mistakes.

    manifest.json   Ensure that the file exists and is a valid gear manifest.
    pyproject.toml  Ensure that the file exists and that
                    - the package directory is 'fw_gear_<GEAR_NAME_FROM_MANIFEST>'
                    - the package name is 'fw-gear-<GEAR_NAME_FROM_MANIFEST>'
                    - the package version is '<GEAR_VERSION_FROM_MANIFEST>'
    .dockerignore   Ensure that the file exists and that
                    - starts with excluding everything using **
                    - re-includes the package directory
  files: manifest.json
  always_run: true
  language: script
  require_serial: true
  pass_filenames: false
  entry: scripts/run.sh hook gearcheck

- id: helmcheck
  name: helmcheck
  description: |
    Run a suite of tools on the given helm chart dir (default: ./helm/<proj>).
    - Run 'helm-docs' to generate helm chart's README.md from values.yaml
    - Run 'helm dependency update' to ensure chart deps are up-to-date
    - Fix common errors and standardize skaffold.yaml if it exists
    - For each test values file at ./helm/<proj>/tests/<test>.yaml, run:
      - Run 'helm template --values <test>.yaml' to render the manifests
      - Run 'helm lint --values <test>.yaml' to lint the chart
      - Run 'kubeconform' on every render to validate against the k8s schema
      - Run 'kubesec' on every render to list vulnerabilities
      - Run 'yamllint' on every render for formatting consistency

    Test values are useful for exercising conditional logic in the templates
    and enabling the rendering of charts with explicitly required values.
    If skaffold.yaml exists, its setValues are used as test values.
    If there are no tests, the chart will be tested using defaults.
  files: helm/|skaffold.yaml
  language: script
  require_serial: true
  pass_filenames: false
  entry: scripts/run.sh hook helmcheck

- id: jsonlint
  name: jsonlint
  description: |
    Ensure JSON files are syntactically valid and consistently formatted.
  types: [json]
  language: script
  require_serial: true
  entry: scripts/run.sh hook jsonlint --in-place --trailing-newline

- id: linkcheck
  name: linkcheck
  description: |
    Check that all links in text files - including comments - are accessible.
  types: [text]
  language: script
  require_serial: true
  entry: scripts/run.sh hook linkcheck

- id: markdownlint
  name: markdownlint
  description: |
    Check markdown files for syntax and enforce uniform style.
  types: [markdown]
  exclude: helm/.*/README.md
  language: script
  require_serial: true
  entry: scripts/run.sh hook markdownlint --fix

- id: poetry_export
  name: poetry_export
  description: |
    Export python deps from poetry.lock or pyproject.toml to requirements.txt.
    Allows managing deps with poetry but simply installing with pip in docker.
  files: pyproject.toml|poetry.lock
  language: script
  require_serial: true
  pass_filenames: false
  entry: scripts/run.sh hook poetry_export

- id: ruff
  name: ruff
  description: |
    Lint python files with ruff, ensuring consistent docs, import sort and more.
    To disable pydocstyle errors on undocumentet code, override the args with [].
  types: [python]
  exclude: ^tests/|^migrations/
  language: script
  require_serial: true
  entry: scripts/run.sh hook ruff check --fix --unsafe-fixes
    --select=E,F,I,PL --ignore=E501,PLC1901,PLR2004,PLW2901
  args: [
    "--select=D",
    "--ignore=D203,D204,D213,D215,D400,D401,D404,D406,D407,D408,D409,D413"
  ]

- id: ruff_tests
  name: ruff_tests
  description: |
    Lint python tests with ruff, using less strict rules compared to project code.
  types: [python]
  files: ^tests/
  language: script
  require_serial: true
  entry: scripts/run.sh hook ruff check --fix --unsafe-fixes
    --select=E,F,I,PL --ignore=E501,PLC1901,PLR0913,PLR2004,PLW2901

- id: ruff_format
  name: ruff_format
  description: |
    Auto-format python source code.
  types: [python]
  language: script
  require_serial: true
  entry: scripts/run.sh hook ruff format

- id: shellcheck
  name: shellcheck
  description: |
    Lint shell scripts for common mistakes and style.
  types: [shell]
  language: script
  require_serial: true
  entry: scripts/run.sh hook shellcheck
  args: [--color=always, --external-sources]

- id: yamllint
  name: yamllint
  description: |
    Ensure YAML files are syntactically valid and consistently formatted.
  types: [yaml]
  exclude: helm
  language: script
  require_serial: true
  entry: scripts/run.sh hook yamllint
  args: [--format=colored]

# DEPRECATED HOOKS
# KEEPING UNTIL A VIABLE ALTERNATIVE BECOMES AVAILABLE

- id: mypy
  name: mypy
  types: [python]
  exclude: tests
  language: script
  require_serial: true
  entry: scripts/run.sh hook mypy
  args: [
    --ignore-missing-imports,  # ignore imports of missing modules (TODO)
    --implicit-optional,       # do not require explicit Optional in signatures
    --install-types,           # auto-install type stubs (eg. types-requests)
    --non-interactive,         # do not prompt for installing type stubs
    --color-output,            # colors, yes please
    --pretty,                  # show code snippets and error markers
    --show-column-numbers,
    --show-error-context,
    --show-error-codes,
  ]

- id: pytest
  name: pytest
  types: [python]
  files: ^tests/
  language: script
  always_run: true
  require_serial: true
  pass_filenames: false
  entry: scripts/pytest_local.sh
  args: [
    tests,                          # standard test path
    --color=yes,                    # colors, yes please
    --durations=5,                  # show the 5 slowest tests w/ timing
    --no-cov-on-fail,               # don't report coverage when a test fails
    --cov-report=term-missing,      # report the non-covered lines in a table
    --cov-report=xml:coverage.xml,  # also create a full xml coverage report
    --junitxml=junit.xml,           # and a junit/xunit2 one, too
    -o=junit_family=xunit2,         # set the junit style for gitlab compat
  ]
