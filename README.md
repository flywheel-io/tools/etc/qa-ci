# QA/CI - pre-commit hooks and GitLab CI templates

Standardized development workflows and continuous integration pipelines using

- [pre-commit](https://pre-commit.com/) for running linters locally
- [GitLab CI](https://docs.gitlab.com/ee/ci/) for doing the same in CI
  then publishing artifacts and running further automation.

## Table of Contents

[TOC]

## Usage

1. Ensure you have `bash`, `docker` and `pre-commit` installed:

    ```bash
    brew install bash docker pre-commit
    ```

2. Create a `.pre-commit-config.yaml` in your project:<br/>
    _This is the recommended hook configuration for containerized python apps._<br/>
    _Check out the [pre-commit](#pre-commit) section for details and options._

    ```yaml
    repos:
      - repo: https://gitlab.com/flywheel-io/tools/etc/qa-ci
        rev: main
        hooks:
          - id: eolfix
          - id: hadolint
          - id: helmcheck
          - id: jsonlint
          - id: linkcheck
          - id: markdownlint
          - id: poetry_export
          - id: ruff
          - id: ruff_format
          - id: ruff_tests
          - id: shellcheck
          - id: yamllint
    ```

3. Create a `.gitlab-ci.yml` in your project:<br/>
    _This is the recommended CI configuration for containerized python apps._<br/>
    _Check out the [GitLab CI](#gitlab-ci) section for details and options._

    ```yaml
    include:
      - project: flywheel-io/tools/etc/qa-ci
        ref: main
        file: ci/app.yml
    ```

4. Update the qa-ci reference in both files from `main` to a non-mutable rev:<br/>
    _The pre-commit and the gitlab-ci revs should always be pinned and in sync._

    ```bash
    pre-commit try-repo https://gitlab.com/flywheel-io/tools/etc/qa-ci autoupdate
    ```

5. Profit! Your project can now be tested using pre-commit and CI is configured
    to run the same tests and to publish any project artifacts as needed.

    ```bash
    pre-commit run -a         # run all the hooks
    pre-commit install        # auto-run hooks before you commit
    ```

## pre-commit

Hooks help identifying issues before submission to code review. Running hooks
before every commit allows pointing out problems like invalid YAML syntax,
non-standard code formatting or broken tests. Fixing these before code review
allows reviewers to focus on the architecture of the change while not wasting
time on style nitpicks or worrying about whether the tests pass.

Using pre-commit provides a single, unified entry point for all building, linting
and testing activities related to a project's development lifecycle, all driven
from `.pre-commit-config.yaml`. Running the same command to invoke the same or
similar tasks lowers friction when switching between projects and facilitates
code standardization as well as CI automation.

### Hook configuration

- Hooks are configured with `.pre-commit-config.yaml`.<br/>
  Projects using the py/docker/helm stack should use the **recommended hooks**
  listed in the [Usage](#usage) section above and documented in detail in the
  [Supported hooks](#supported-hooks) section below.

- Hooks come with a **recommended configuration** to aid in code standardization
  across projects. You can override the defaults by passing your own hook args:

    ```yaml
    hooks:
      - id: ruff
        args: [--select, "PL"]  # override qa-ci's default
    ```

- You can inspect a CLI tools' help text to discover their available args:

    ```bash
    docker run -it --rm flywheel/qa-ci ruff --help
    ```

- Hooks **can be disabled** by commenting them out, which is useful for adopting
  them gradually in existing projects:

    ```yaml
    hooks:
    # - id: shellcheck  # disable shellcheck by commenting it out
    ```

- Hooks **can be extended** with [3rd party hooks](https://pre-commit.com/hooks.html)
  and custom [local](https://pre-commit.com/#repository-local-hooks) entries:

    ```yaml
    # add 3rd party hook after the qa-ci ones:
    - repo: https://github.com/pre-commit/pre-commit-hooks
      rev: v4.3.0
      hooks:
        - id: trailing-whitespace
    ```

- Finally, hooks **can be removed** if they are not relevant to your project.

### Supported hooks

This section describes the qa-ci hooks' behavior and configuration in detail.
You can inspect the supported hook entries and their default arguments in
[`.pre-commit-hooks.yaml`](.pre-commit-hooks.yaml).<br/>
Qa-ci hooks are containerized to minimize dev environment setup time and to
increase reproducibility across developer machines and CI runners.<br/>
Hooks may generate new files or reformat existing files in place. Eg.: ruff format
may change whitespace in python code.<br/>
When this happens to tracked files, pre-commit will consider that hook failed.
To fix this, simply commit that diff and re-run the hook.

#### eolfix

Fix `text` files (i.e.: any source code) to enforce LF line endings and to ensure
a single LF at EOF (no more, no less).

Links:

- [scripts/eolfix.py](scripts/eolfix.py)

#### hadolint

Lint `Dockerfile` to enforce best practices with hadolint. Uses shellcheck under
the hood to lint the `RUN` instructions as well.

Links:

- [hadolint](https://github.com/hadolint/hadolint)
- [Dockerfile best practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)

#### helmcheck

Document, lint, test and security-scan helm charts with a custom qa-ci script
`helmcheck.sh`. Runs helm-docs to generate the chart README, then renders the
templates with test values to run yamllint, helm lint, kubeconform and kubesec on
the rendered YAMLs.

Test values files are useful for templating charts that have required values
and/or test-covering the templating logic on various if/else branches.
Custom test values can be placed in the `helm/<project>/tests/` dir, eg:
  `helm/my-app/tests/test_nodeport.yaml`
If there are no custom tests, the chart will be tested using defaults.
The overrides of skaffold.yaml (if present) are also used for testing.

Links:

- [scripts/helmcheck.sh](scripts/helmcheck.sh)
- [helm-docs](https://github.com/norwoodj/helm-docs)
- [helm lint](https://helm.sh/docs/helm/helm_lint/)
- [kubeconform](https://github.com/yannh/kubeconform)
- [kubesec](https://github.com/controlplaneio/kubesec)
- [Helm best practices](https://gitlab.com/flywheel-io/infrastructure/sre/-/blob/master/guides/BEST_PRACTICES/HELM_BEST_PRACTICES.md)
- [Helm template functions](https://helm.sh/docs/chart_template_guide/function_list/)
- [Sprig template functions](https://masterminds.github.io/sprig/)

#### jsonlint

Lint JSON files for syntax, uniform indentation and style with jsonlint.
Formats `.json` files in-place to consistently use 2 spaces for indentation.

Links:

- [jsonlint](https://github.com/kevcenteno/jsonlint)

#### linkcheck

Check text files for dead links with a custom qa-ci script `linkcheck.py`.

Looks for HTTP links in any text file using regex and attempts to issue a HEAD
request on all the URLs found. Ignores links

- without a top level domain (eg. `localhost`)
- with top level domain `local` and `test`
- with domain `helm.dev.flywheel.io` and `local.flywheel.io`
- matching any of the `--ignore` patterns passed as arguments

When checking links, strip URL fragments and consider:

- `2xx`, `401` and `403` as success (assume unauthorized responses are OK)
- `503` as success for gitlab links (which return 503 on private repos)
- everything else as failure

In addition to HTTP URLs, check markdown header / file references.

Links:

- [scripts/linkcheck.py](scripts/linkcheck.py)
- [HTTP response status codes](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)

#### markdownlint

Lint markdown files for syntax, line length, style and more with markdownlint.
Re-formats `.md` files in-place to consistently use indentation and newlines.

- The CLI arguments (hook args) can be used to enable/disable linter rules
- To access all config options, you need to use a config file named `.markdownlint-cli2.yaml`
- On projects without a config, defaults to the qa-ci config file that sets the max
  line length to `88` and allow inline HTML like `<br/>`.

Links:

- [markdownlint](https://github.com/DavidAnson/markdownlint)
- [.markdownlint-cli2.yaml (qa-ci)](.markdownlint-cli2.yaml)
- [.markdownlint-cli2.yaml (sample)](https://github.com/DavidAnson/markdownlint-cli2/blob/main/schema/markdownlint-cli2-config-schema.json)
- [Markdown cheatsheet](https://devhints.io/markdown)
- [Markdown editor](https://stackedit.io/app#)
- [GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html)
- [Mermaid cheatsheet](https://jojozhuang.github.io/tutorial/mermaid-cheat-sheet/)
- [Mermaid editor](https://mermaid-js.github.io/mermaid-live-editor/edit)

#### poetry_export

Export python dependencies from `pyproject.toml` to `requirements.txt` with poetry.

Python project dependencies **should** be managed with poetry.
Exporting the required packages with their versions pinned into the simpler
requirements.txt format allows installing them pip:

```bash
pip install -r requirements.txt
```

This can be useful for writing a `Dockerfile` without installing poetry for example,
since it's a large, hard-to-containerize utility.

- Prod dependencies will be exported into `requirements.txt`.
- Dev-only dependencies will be exported into `requirements-dev.txt`.
- Extras named `all` will also be included in `requirements-dev.txt`.
- If there are _any_ extras, `all` is required for this hook to work.

Links:

- [poetry](https://github.com/python-poetry/poetry)
- [scripts/poetry_export.sh](scripts/poetry_export.sh)

#### ruff

Lint python code with static analyzer ruff.
Supersedes isort, pylint and pydocstyle.

Links:

- [ruff](https://github.com/astral-sh/ruff)
- [isort](https://github.com/PyCQA/isort)
- [pydocstyle](https://github.com/PyCQA/pydocstyle)
- [Google Python Style Guide](https://google.github.io/styleguide/pyguide.html)
- [Example Google Style Docstring](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)

#### ruff_format

Format python code with ruff to ensure uniform whitespace and style.

Links:

- [ruff format](https://docs.astral.sh/ruff/formatter/)

#### ruff_tests

Lint python tests using less strict defaults with static analyzer ruff.

#### shellcheck

Lint shell scripts with the static analyzer shellcheck.

Links:

- [shellcheck](https://github.com/koalaman/shellcheck)
- [Bash best practices](https://kvz.io/blog/bash-best-practices.html)
- [Bash cheatsheet](https://quickref.me/bash)
- [Shell parameter expansion](https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html)

#### yamllint

Lint YAML files for syntax, uniform indentation and style with yamllint. Formats
`.yaml` files in-place to consistently use 2 spaces as indentation and to enforce
maximum line length `88` - among many other rules.

Links:

- [yamllint](https://github.com/adrienverge/yamllint)
- [YAML cheatsheet](https://quickref.me/yaml)
- [YAML multiline strings](https://yaml-multiline.info/)

## GitLab CI

<!-- TODO image -->
<!-- TODO runner -->
<https://gitlab.com/flywheel-io/infrastructure/deployments/flywheel-build/build-cluster>

GitLab [CI stages](https://docs.gitlab.com/ee/ci/yaml/#stage) allow grouping jobs
into different stages where jobs in the same stage are executed in parallell but
the stages are run one after the other, making their order important.

All jobs defined in CI templates are assigned to a stage. The complete list of
stages currently in use across the qa-ci templates are:

```yaml
stages:
  - test     # run pre-commit
  - publish  # push artifacts (docker/helm/poetry/pages)
  - release  # automatic release mr and repo tagging using the bot
  - update   # automatic repo updates and external project triggers
```

### Pipelines

Qa-ci comes with full, off-the-shelf pipelines for application components,
standalone docker images and python libraries.<br/>
Projects matching any of these use cases should use an off-the-shelf pipeline.
Standardizing CI pipelines lowers friction when switching between projects and
facilitates more and better automation.

#### Flywheel application components

For python applications with a `Dockerfile` and a Helm chart that integrate into
the Flywheel [umbrella](https://gitlab.com/flywheel-io/infrastructure/umbrella)
chart as a sub-compontent.<br/>
_Source: [ci/app.yml](ci/app.yml)_

  ```yaml
  include:
    - project: flywheel-io/tools/etc/qa-ci
      ref: main
      file: ci/app.yml
  ```

#### Docker base and utility images

For projects with a `Dockerfile` that are not tied to an application component,
but contain utilities (or just useful layers) that are distributed as a docker
image on [Docker Hub](https://hub.docker.com/) under the flywheel repository.<br/>
_Source: [ci/img.yml](ci/img.yml)_

```yaml
include:
  - project: flywheel-io/tools/etc/qa-ci
    ref: main
    file: ci/img.yml
```

#### Python libraries

For open-source python libraries published on [PyPI](https://pypi.org/) and for
private ones on [GitLab Package Registry](https://docs.gitlab.com/ee/user/packages/pypi_repository/).<br/>
_Source: [ci/lib.yml](ci/lib.yml)_

  ```yaml
  include:
    - project: flywheel-io/tools/etc/qa-ci
      ref: main
      file: ci/lib.yml
  ```

### Job templates

CI job templates allow selecting and/or customizing your jobs instead of relying
on any set of jobs defined in one of the pipeline templates listed above.

To use a job template, include [ci/templates.yml](ci/templates.yml) and extend it:

  ```yaml
  include:
    - project: flywheel-io/tools/etc/qa-ci
      ref: main
      file: ci/templates.yml

  build:docker:
    extends: .build:docker
  ```

#### .build:docker

Build the project's Docker image and push it to the GitLab container registry
as `$CI_REGISTRY_IMAGE:$CI_PIPELINE_ID`. The registry feature must be enabled
in the project settings `General > Visibility, project features, permissions`.<br/>
Runs on every MR, main branch commit and tag if a `Dockerfile` exists.

#### .test:pre-commit

Run the project's pre-commit hooks as configured in the `.pre-commit-config.yaml`.
The job is pre-configured to support pytest coverage reports.

#### .publish:docker

Pull the build image from the GitLab registry and tag and push it to Docker Hub.
Runs on every MR, main branch commit and tag if a `Dockerfile` exists.
The images are tagged using the following patterns based on the commit source:

| Source  | Pattern       | Example             | Type      |
| ------- | ------------- | ------------------- | --------- |
| tag     | `<TAG>`       | `1.2.3`             | immutable |
| main    | `latest`      | `latest`            | rolling   |
| main/MR | `<REF>`       | `FLYW-123`          | rolling   |
| main/MR | `<REF>.<SHA>` | `FLYW-123.d34db33f` | immutable |
| main/MR | `<SHA>`       | `d34db33f`          | immutable |

\* `<REF>` is the branch name and `<SHA>` is the commit SHA.

Configuration:

| Variable       | Default              | Description              |
| -------------- | -------------------- | ------------------------ |
| `DOCKER_FILE`  | `Dockerfile`         | Dockerfile name to build |
| `DOCKER_IMAGE` | `flywheel/<project>` | Image name to tag with   |

#### .publish:helm

Build and push the project's helm chart to the
[Flywheel ChartMuseum](https://helm.dev.flywheel.io/index.yaml) and the
[GitLab Package Registry](https://docs.gitlab.com/user/packages/helm_repository/).
Runs on every MR, main branch commit and tag if a `helm/*/Chart.yaml` exists.
The chart is versioned using the following patterns based on the commit source:

<!-- markdownlint-disable MD013 -->
| Source | Pattern                           | Example                                | Type      |
| ------ | --------------------------------- | -------------------------------------- | --------- |
| tag    | `<TAG>`                           | `1.2.3`                                | immutable |
| main   | `<VER>-beta.<BUILD>+<REF>.<SHA>`  | `1.2.3-beta.456789+main.d34db33f`      | immutable |
| MR     | `<VER>-alpha.<BUILD>+<REF>.<SHA>` | `1.2.3-alpha.456789+FLYW-123.d34db33f` | immutable |
<!-- markdownlint-enable MD013 -->

\* `<VER>` is the last version and `<BUILD>` is the `$CI_PIPELINE_ID`.

Configuration:

| Variable    | Default | Description                                          |
| ----------- | ------- | ---------------------------------------------------- |
| `GIT_DEPTH` | `100`   | No. of commits to fetch from the history for `<VER>` |

#### .publish:pages

Publish static HTML project documentation from the `public/` folder on
[GitLab pages](https://docs.gitlab.com/ee/user/project/pages/).
Runs on every main branch commit and tag if the `public/` dir exists.

Configuration:

TODO ...

#### .publish:poetry

Publish a python library to PYPI, the Python Package Index.

#### .release:mr

Create a `release-X.Y.Z` branch and MR with the project version bumped in all
files referencing it like `pyproject.toml` and the helm chart.
Run when manually triggering a pipeline with a `RELEASE=X.Y` variable,
where `X.Y` is the desired minor version. The patch version will be determined
automatically.

The MR description will hold a change-log based on the MRs merged since the last
version and the list of JIRA tickets referenced across those changes. If
`FW_COMPONENT` is set, it will also reference any open umbrella release MRs.

Edit the change-log in the MR description to adjust the upcoming project release
docs and check/uncheck any umbrella release MRs to select which to bump.

Configuration:

| Variable       | Default | Description                                    |
| -------------- | ------- | ---------------------------------------------- |
| `FW_COMPONENT` | `""`    | Set to `true` to enable umbrella version bumps |

#### .release:tag

Create an annotated full semver (i.e.: `<major>.<minor>.<patch>`) tag.<br/>
Runs on main and hotfix branches when a `release-X.Y.Z` branch is merged.

The new tag will kick off further pipelines for publishing release artifacts.

#### .update:deps

Create an `update-repo` branch and MR with common project files auto-updated.
Runs on any branch if `UPDATE=true` is set.<br/>
It's recommended to run updates on the main branch every 2-4 weeks using a
[scheduled pipeline](https://docs.gitlab.com/ee/ci/pipelines/schedules.html)
with a cron string like `0 0 * * sun%4` for every 4th Sunday.

| File                      | Description                                    |
| ------------------------- | ---------------------------------------------- |
| `Dockerfile`              | Update the base image in `FROM`                |
| `pyproject.toml`          | Update the build-system and loosen `^0.X` pins |
| `poetry.lock`             | Update the lock file from pyproject.toml       |
| `requirements.txt`        | Update requirements files                      |
| `.pre-commit-config.yaml` | Update pre-commit repo references              |
| `.gitlab-ci.yml`          | Update gitlab-ci include references            |

Configuration:

| Variable      | Default | Description                                     |
| ------------- | ------- | ----------------------------------------------- |
| `UPDATE_SKIP` | `""`    | Space-separated list of files **not** to update |

#### .update:release

<!-- TODO add links in CI job log to project release -->
Create a GitLab [project release](https://docs.gitlab.com/ee/user/project/releases/)
with the change-log and JIRA tickets from the release MR description.<br/>
If `FW_COMPONENT` is set and any umbrella releases were selected on the release
MR description, then also bump the component version on those.<br/>
Runs on tags only.

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
