ARG PYTHON_VERSION=3.13

FROM flywheel/python:build AS opt
SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]
RUN eget chartmuseum/helm-push -f'bin/*';   helm-cm-push --help &>/dev/null
RUN eget christian-korneck/docker-pushrm;   docker-pushrm --help &>/dev/null
RUN eget controlplaneio/kubesec;            kubesec --help &>/dev/null
RUN eget GoogleContainerTools/skaffold;     skaffold --help &>/dev/null
RUN eget hadolint/hadolint;                 hadolint --help &>/dev/null
RUN eget norwoodj/helm-docs -atar;          helm-docs --help &>/dev/null
RUN eget shteou/helm-dependency-fetch;      helm-dependency-fetch --help &>/dev/null
RUN eget yannh/kubeconform;                 kubeconform -h &>/dev/null

FROM flywheel/python:$PYTHON_VERSION-build
SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]
RUN apk --no-cache add \
        bat gitlab-release-cli glab helm jo ncurses npm parallel ruff shellcheck shfmt sops \
        cni-plugins docker-cli-buildx docker-cli-compose fuse-overlayfs podman skopeo \
    ; \
    uv pip install --upgrade \
        pip setuptools wheel \
        poetry poetry-plugin-export pre-commit \
    ; \
    npm install --location=global --omit=dev \
        @prantlf/jsonlint markdownlint-cli2 npm prettier pyright \
    ; \
    rm -rf ~/.{cache,npm} /tmp/*
COPY --from=opt /opt /opt
RUN mkdir -p /etc/containers; \
    xh -IFo /etc/containers/containers.conf https://raw.githubusercontent.com/containers/image_build/main/podman/containers.conf; \
    sed -Ei /etc/containers/registries.conf \
        -e's|^# ?(unqualified-search-registries) = .*|\1 = ["docker.io", "quay.io"]|'; \
    sed -Ei /etc/containers/storage.conf \
        -e's|^# ?(mount_program)|\1|' \
        -e's|^(mountopt) = .*|\1 = "nodev,fsync=0"|' \
        -e'/^additionalimagestores = .*/a "/var/lib/shared",'; \
    mkdir -p /var/lib/shared/{overlay,vfs}-{images,layers}; \
    touch /var/lib/shared/{overlay,vfs}-{images/images,layers/layers}.lock
ENV BUILDAH_FORMAT=docker \
    BUILDX_EXPERIMENTAL=1 \
    DOCKER_BUILDKIT=1 \
    GIT_TERMINAL_PROMPT=0 \
    LANG=C.UTF-8 \
    LANGUAGE=en_US \
    LC_ALL=C.UTF-8 \
    NPM_CONFIG_FUND=false \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_ROOT_USER_ACTION=ignore \
    POETRY_INSTALLER_MAX_WORKERS=10 \
    POETRY_VIRTUALENVS_CREATE=false \
    POETRY_VIRTUALENVS_IN_PROJECT=false \
    POETRY_WARNINGS_EXPORT=false

WORKDIR /qa-ci
ENV QA_CI=true COMMIT_SHA=dev
COPY requirements.txt ./
RUN uv pip install -rrequirements.txt
COPY . .
RUN uv pip install --no-deps -e.
ENTRYPOINT []
CMD ["bash"]
